import './App.css';
import React, {Fragment, useState, useEffect} from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// components
import Login from "./components/Login";
import DoctorSideMenu from "./components/DoctorSideMenu";
import PatientSideMenu from "./components/PatientSideMenu";
import AdminSideMenu from "./components/AdminSideMenu";
import NurseSideMenu from "./components/NurseSideMenu";
import NoMatch from "./components/404Page";
import {UserContext} from "./UserContext";
import {makeStyles} from "@material-ui/core/styles";
import coverImage from "./cover.jpg";
import {fade} from "@material-ui/core/styles/colorManipulator";
import {getAUser} from "./api/Api";


const useStyles = makeStyles((theme) => ({
    //Drawer side menu
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: 240,
        width: `calc(100% - 240px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: 240,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: 240,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4)
    },
    link: {
        textDecoration: "none",
        color: theme.palette.text.primary
    },
    //input fields
    inputField: {
        margin: theme.spacing(1)
    },
    inputFieldHalf: {
        margin: theme.spacing(1),
        width: "14rem"
    },
    //list patients, medical staffs and DA
    card: {
        width: 275,
        margin: theme.spacing(2)
    },
    cardBtn: {
        justifyContent: "center"
    },
    cardBtnItem: {
        margin: theme.spacing(1),
    },
    btnItem: {
        marginLeft: theme.spacing(2)
    },
    chip: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    [theme.breakpoints.up('xs')]: {
        //for login page background
        loginBackground: {
            backgroundColor: "#436170",
            backgroundImage: 'url(' + coverImage + ')',
            backgroundPosition: 'contain',
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover"
        },
        mainTitle: {
            backgroundColor: "#d5dcdf"
        },
    },
    [theme.breakpoints.up('md')]: {
        //for login page background
        loginBackground: {
            backgroundColor: "#436170",
            backgroundImage: 'url(' + coverImage + ')',
            backgroundPosition: 'left',
            backgroundRepeat: "no-repeat",
            backgroundSize: "65vw 100vh",
        },
        mainTitle: {
            position: "relative",
            left: -30,
            backgroundColor: "#d5dcdf"
        },
    },
    fabContainer: {
        position: "fixed",
        zIndex: 5,
        right: theme.spacing(5),
        bottom: theme.spacing(11)

    },
    fab: {
        marginRight: theme.spacing(2),
        '&:hover': {
            boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
        },
    },
    // others
    addMargin: {
        margin: theme.spacing(1)
    },
    addPaddingBottom: {
        paddingBottom: theme.spacing(5)
    },
    // dashboard
    dashboard: {
        backgroundColor: "#f0f0f0"
    },
    table: {
        minWidth: 650,
    },
    whiteText: {
        color: "#FFFFFF"
    },
    cardContainer: {
        background: "linear-gradient(to bottom, #2A4857 0%, #436170 75%, #5D7B8A 100%)",
        padding: theme.spacing(2),
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100%"
    },
    cardContent: {
        color: "#FFFFFF",
        width: "50%"
    },
    overlay: {
        maxHeight: "350px"
    },
    primaryBackground: {
        background: "linear-gradient(to bottom, #2A4857 0%, #436170 75%, #5D7B8A 100%)",
        padding: theme.spacing(2),
        height: "100%"
    },
    secondaryBackground: {
        background: "linear-gradient(to bottom, #DE5353 0%, #f76c6c 75%, #FF8686 100%)",
        padding: theme.spacing(2),
        height: "100%"
    },
    ul: {
        listStyle: "inside",
        paddingLeft: 0
    },
    square: {
        display: "inline-block",
        width: "20px",
        height: "20px",
        border: "solid 1px white",
        marginRight: theme.spacing(1),
    },
    changeResult: {
        padding: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    changeResult2: {
        padding: theme.spacing(1),
        marginBottom: theme.spacing(1),
        backgroundColor: "#f76c6c",
        color: "#FFFFFF"
    },
    whiteDashboard: {
        padding: theme.spacing(2)
    },
    divider: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    stress: {
        backgroundColor: "rgba(129, 0, 38, 0.3)",
        color: "#4E0000",
        border: "solid #4E0000 1.5px",
        boxShadow: "0.5px 0.5px 4px 1px #810026"

    },
    normal: {
        backgroundColor: "rgba(255, 166, 86, 0.4)",
        color: "#FFD989",
        border: "solid #FFD989 1.5px",
        boxShadow: "1px 1px 12px 1px #FFA656"
    },
    normalHeart: {
        backgroundColor: "rgba(127, 206, 187, 0.4)",
        color: "#7fcebb",
        border: "solid #7fcebb 1.5px",
        boxShadow: "0.5px 0.5px 4px 1px #7FCEBB"
    },
    murmur: {
        backgroundColor: "rgba(34, 94, 167, 0.4)",
        color: "#6FABF4",
        border: "solid #6FABF4 1.5px",
        boxShadow: "1px 1px 12px 1px #225EA7"
    },
    extra: {
        backgroundColor: "rgba(59, 79, 138, 0.4)",
        color: "#00033E",
        border: "solid #00033E 1.5px",
        boxShadow: "1px 1px 12px 1px #3B4F8A"
    },
    // appointments
    calendarGrid: {
        border: "1px solid #a6a6a6",
        width: "100%"
    },
    todayCell: {
        backgroundColor: fade(theme.palette.primary.main, 0.1),
        '&:hover': {
            backgroundColor: fade(theme.palette.primary.main, 0.14),
        },
        '&:focus': {
            backgroundColor: fade(theme.palette.primary.main, 0.16),
        },
    },
    disableCell: {
        backgroundColor: fade(theme.palette.action.disabledBackground, 0.02),
        '&:hover': {
            backgroundColor: fade(theme.palette.action.disabledBackground, 0.02),
        },
        '&:focus': {
            backgroundColor: fade(theme.palette.action.disabledBackground, 0.02),
        },
    },
    today: {
        backgroundColor: fade(theme.palette.primary.main, 0.2),
    },
    weekend: {
        backgroundColor: fade(theme.palette.action.disabledBackground, 0.06),
    },
}));


function App() {
    const classes = useStyles();
    const [ isAuthenticated, setIsAuthenticated ] = useState(false);
    const [ currentUser, setCurrentUser ] = useState({});

    const setAuth = boolean => {
        setIsAuthenticated(boolean);
    };

    const isAuth = async () => {
        try {
            const response = await fetch("http://localhost:5000/auth/isVerify", {
                method: "GET",
                headers: { token: localStorage.token }
            });

            const parseRes = await response.json();
            if (parseRes === true) {
                await getAUser(localStorage.id).then(res => setCurrentUser(res));
                return true;
            } else {
                setCurrentUser({});
                return false;
            }
        } catch (err){
            console.error(err.message);
        }
    }

    useEffect(() => {
        isAuth().then(res => setIsAuthenticated(res));
    },[isAuthenticated]);

    return (
      <Fragment>
          <Router>
              <Switch>
                  {
                      !isAuthenticated ?
                          <Route exact path="/" render={() => <Login setAuth={setAuth} classes={classes} />} />
                          :
                          <UserContext.Provider value={ currentUser }>
                          <div className={classes.root}>
                          <Route path="/" render={
                              currentUser.user_role==="D" ? () => <DoctorSideMenu setAuth={setAuth} classes={classes} />
                              : currentUser.user_role==="A" ? () => <AdminSideMenu setAuth={setAuth} classes={classes} />
                              : currentUser.user_role==="P" ? () => <PatientSideMenu setAuth={setAuth} classes={classes} />
                              : currentUser.user_role==="N" ? () => <NurseSideMenu setAuth={setAuth} classes={classes} />
                              : () => <NoMatch />
                          } /> </div></UserContext.Provider>
                  }
                  <Route path='*' component={NoMatch}/>
              </Switch>
          </Router>
      </Fragment>
    );
}

export default App;
