import React, {Fragment, useEffect, useState} from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AssessmentIcon from '@material-ui/icons/Assessment';

// Components
import ListPatients from "./ListPatients";
import ListDoctors from "./ListDoctors";
import ListNurse from "./ListNurse";
import Report from "./Report";
import Box from "@material-ui/core/Box";


toast.configure();

const AdminSideMenu = ({setAuth, classes}) => {
    const [open, setOpen] = useState(false);
    const [index, setIndex] = useState(0);

    const logout = async e => {
        e.preventDefault();
        try {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            setAuth(false);
            window.location.replace("/");
            toast.success("Logout successfully!");
        } catch (err){
            console.error(err.message);
        }
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleClickMenuItem = index => {
        setIndex(index);
    };

    const setDefaultIndex = () => {
        if(window.location.pathname === "/listMedStaffs"){
            setIndex(1);
        } else if(window.location.pathname === "/listNurses"){
            setIndex(2);
        } else {
            setIndex(0);
        }
    }

    useEffect(() => {
        setDefaultIndex();
    }, []);

    return (
        <Fragment>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit" aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}>
                        <MenuIcon />
                    </IconButton>

                    <Box width="100%">
                        <Grid container justify="space-between">
                            <Grid item>
                                <Typography variant="h4" noWrap>Admin Dashboard</Typography>
                            </Grid>
                            <Grid item>
                                <Button onClick={e => logout(e)} color="inherit">Logout</Button>
                            </Grid>
                        </Grid>
                    </Box>

                </Toolbar>
            </AppBar>

            <Router>
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: open,
                            [classes.drawerClose]: !open,
                        }),
                    }}>
                    <div className={classes.toolbar}>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <MenuList>
                        <Link className={classes.link} to="/">
                            <MenuItem button selected={index === 0} onClick={() => handleClickMenuItem(0)}>
                                <ListItemIcon>
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography primary="Patients" />
                                </Typography>

                            </MenuItem>
                        </Link>
                        <Link className={classes.link} to="/listDoctors">
                            <MenuItem button selected={index === 1} onClick={() => handleClickMenuItem(1)}>
                                <ListItemIcon>
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography primary="Doctors" />
                                </Typography>

                            </MenuItem>
                        </Link>
                        <Link className={classes.link} to="/listNurses">
                            <MenuItem button selected={index === 2} onClick={() => handleClickMenuItem(2)}>
                                <ListItemIcon>
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography primary="Nurses" />
                                </Typography>

                            </MenuItem>
                        </Link>
                        <Link className={classes.link} to="/monthlyReport">
                            <MenuItem button selected={index === 3} onClick={() => handleClickMenuItem(3)}>
                                <ListItemIcon>
                                    <AssessmentIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography primary="Report" />
                                </Typography>

                            </MenuItem>
                        </Link>
                    </MenuList>
                </Drawer>

                <main className={classes.content}>
                    <div className={classes.toolbar} />
                <Switch>
                    <Route exact path="/">
                        <Fragment>
                            <ListPatients classes={classes}/>
                        </Fragment>

                    </Route>
                    <Route exact path="/listDoctors">
                        <Fragment>
                            <ListDoctors classes={classes}/>
                        </Fragment>

                    </Route>
                    <Route exact path="/listNurses">
                        <Fragment>
                            <ListNurse classes={classes}/>
                        </Fragment>
                    </Route>
                    <Route exact path="/monthlyReport">
                        <Fragment>
                            <Report classes={classes}/>
                        </Fragment>
                    </Route>
                </Switch>
                </main>
            </Router>
        </Fragment>
    );
}

export default AdminSideMenu;
