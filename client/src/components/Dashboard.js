import React, {Fragment, useState, useEffect, useContext} from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Grid from '@material-ui/core/Grid';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import IconButton from '@material-ui/core/IconButton';
import { ResponsiveHeatMap } from '@nivo/heatmap';
import { ResponsiveLine } from '@nivo/line';
import Paper from "@material-ui/core/Paper";
import moment from "moment";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Divider from "@material-ui/core/Divider";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import FileUpload from "./FileUpload";
import Square from "./Square";
import EcgSignalGraph from "./EcgSignalGraph";
import Download from "./Download";
import {UserContext} from "../UserContext";


toast.configure();

const Dashboard = ({ patient, diseases, classes }) => {
    const currentUser = useContext(UserContext);
    const [ medicalData, setMedicalData ] = useState([]);
    const [ ecg, setEcg ] = useState([]);
    const [ heartsounds, setHeartsounds ] = useState([]);
    const [ habitData, setHabitData ] = useState([]);
    const [ bmi, setBmi ] = useState([]);
    const [ openECG, setOpenEcg ] = useState(false);
    const [ openHeart, setOpenHeart ] = useState(false);
    const [ openHabit, setOpenHabit ] = useState(false);
    const [ habitChartData, setHabitChartData ] = useState({});
    const [ openBMI, setOpenBmi ] = useState(false);
    const [ openSignal, setOpenSignal ] = useState(false);
    const [ currentFilename, setCurrentFilename ] = useState("");
    const [ currentFilepath, setCurrentFilepath ] = useState("");
    const [ patientHeightInM, setPatientHeightInM ] = useState(bmi.height_in_m);
    const [ patientWeightInKg, setPatientWeightInKg ] = useState(bmi.weight_in_kg);
    const [ selectedDate, setSelectedDate ] = useState(new Date());
    const [ downloadDate, setDownloadDate ] = useState("");
    const [ downloadPath, setDownloadPath ] = useState("");
    const [ openDownload, setOpenDownload ] = useState(false);
    const allowEditDashboard = (currentUser.user_role === 'N');
    const [ disableNext, setDisableNext ] = useState(true);
    const [ heightErrorText, setHeightErrorText ] = useState("");
    const [ weightErrorText, setWeightErrorText ] = useState("");
    const isValidForm = useState((heightErrorText === "" && weightErrorText === ""))


    const deleteEcg = async (ecg_id) => {
        if (window.confirm('Confirm delete?')) {
            try {
                const response = await fetch(`http://localhost:5000/ecg/${ecg_id}`, {
                    method: "DELETE",
                    headers: {token: localStorage.token}
                });

                const parseRes = await response.json();

                if (parseRes === "Deleted successfully!") {
                    setEcg(ecg.filter(e => e.ecg_id !== ecg_id))
                    toast.success(parseRes);
                } else {
                    toast.error(parseRes);
                }

            } catch (err) {
                console.error(err.message);
            }
        }
    }

    const deleteHeartsound = async (heartbeat_sound_id) => {
        if (window.confirm('Confirm delete?')) {
            try {
                const response = await fetch(`http://localhost:5000/heartsound/${heartbeat_sound_id}`, {
                    method: "DELETE",
                    headers: {token: localStorage.token}
                });

                const parseRes = await response.json();

                if (parseRes === "Deleted successfully!") {
                    setHeartsounds(heartsounds.filter(heartsound => heartsound.heartbeat_sound_id !== heartbeat_sound_id))
                    toast.success(parseRes);
                } else {
                    toast.error(parseRes);
                }

            } catch (err) {
                console.error(err.message);
            }
        }
    }

    const handleOpenBMIDialog = () => {
        setOpenBmi(true);
    };

    const handleCloseBMIDialog = () => {
        setOpenBmi(false);
        setBmi([]);
    };

    const handleOpenEcgDialog = () => {
        setOpenEcg(true);
    };

    const handleCloseEcgDialog = () => {
        setOpenEcg(false);
        setEcg([]);
    };

    const handleOpenHabitDialog = () => {
        setOpenHabit(true);
    };

    const handleCloseHabitDialog = () => {
        setOpenHabit(false);
        setHabitData([]);
    };

    const handleOpenHeartDialog = () => {
        setOpenHeart(true);
    };

    const handleCloseHeartDialog = () => {
        setOpenHeart(false);
        setHeartsounds([]);
    };

    const handleOpenSignalDialog = (filename, filepath) => {
        setCurrentFilename(filename);
        setCurrentFilepath(filepath);
        setOpenSignal(true);
    };

    const handleCloseSignalDialog = () => {
        setOpenSignal(false);
        setCurrentFilename("");
        setCurrentFilepath("");
    };

    const handleOpenDownloadDialog = date => {
        setDownloadDate(date)

        habitData.forEach(data => {
            if(data.record_date.substr(0, 10) === date){
                setDownloadPath(data.csv_filepath);
            }
        })

        setOpenDownload(true);
    };

    const handleCloseDownloadDialog = () => {
        setOpenDownload(false);
        setDownloadDate("");
        setDownloadPath("");
    };

    const getEcgClass = date => {
        const result = ecg.find(({ record_datetime }) =>
            record_datetime.substr(0, 10) === date)

        if(result===undefined){
            return 0;
        } else {
            if(result.is_stressed){
                return 2;
            }
            else {
                return 1;
            }
        }
    }

    const getHeartSoundClass = date => {
        const result = heartsounds.find(({ record_datetime }) =>
            record_datetime.substr(0, 10) === date)

        if(result===undefined){
            return 0;
        } else {
            if(result.heartsound_class==="Normal"){
                return 1;
            }
            else if(result.heartsound_class==="Murmur"){
                return 2;
            } else {
                return 3;
            }
        }
    }

    const handleHeightChange = e => {
        let height = e.target.value
        let rounded_height = Math.round(height * 100) / 100

        setPatientHeightInM(rounded_height.toString())

        if (parseFloat(height) >= 0 && parseFloat(height) <= 2.5) {
            setHeightErrorText("")
        }
        else {
            setHeightErrorText("Height must be between 0m to 2.5m.")
        }
    };

    const handleWeightChange = e => {
        let weight = e.target.value
        let rounded_weight = Math.round(weight * 10) / 10

        setPatientWeightInKg(rounded_weight.toString())

        if (parseFloat(weight) >= 0 && parseFloat(weight) <= 300) {
            setWeightErrorText("")
        }
        else {
            setWeightErrorText("Weight must be between 0 to 300kg.")
        }
    };

    const handleChangeDate = date => {
        setSelectedDate(date);
        dateIsToday(date) ? setDisableNext(true) : setDisableNext(false);
    };

    const handleIncreaseDate = () => {
        // increase one day from selected date
        selectedDate.setDate(selectedDate.getDate()+1);
        dateIsToday(selectedDate) ? setDisableNext(true) : setDisableNext(false);
    };

    const handleDecreaseDate = () => {
        // decrease one day from selected date
        selectedDate.setDate(selectedDate.getDate()-1);
        dateIsToday(selectedDate) ? setDisableNext(true) : setDisableNext(false);
    };

    const dateIsToday = date => {
        const today = new Date();

        return (
            (date.getFullYear() === today.getFullYear())
            && (date.getMonth() === today.getMonth())
            && (date.getDate() === today.getDate())
            )
    };

    const onSubmitBmiForm = async e => {
            e.preventDefault();

            if(isValidForm) {
                const body = {
                    patient_id: patient.patient_id,
                    record_date: `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`,
                    height_in_m: patientHeightInM,
                    weight_in_kg: patientWeightInKg,
                    current_bmi: Math.round(patientWeightInKg / (patientHeightInM ** 2) * 10000) / 10000,
                    last_bmi: bmi.last_bmi,
                    last_category: bmi.current_category
                };

                try {
                    const response = await fetch("http://localhost:5000/bmi/", {
                        method: "POST",
                        headers: {"Content-Type": "application/json", token: localStorage.token},
                        body: JSON.stringify(body)
                    });

                    const parseRes = await response.json();

                    if (parseRes === "Created successfully!") {
                        toast.success(parseRes);
                    } else {
                        toast.error(parseRes);
                    }

                } catch (err) {
                    console.error(err);
                }

            } else {
                toast.error("Input error.");
            }
    };

    useEffect(() => {
        const getMedicalData = async () => {
            try {
                const response = await fetch(`http://localhost:5000/medicalData/${patient.patient_id}/${selectedDate.getFullYear()}/${selectedDate.getMonth()+1}/${selectedDate.getDate()}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                setMedicalData(jsonData);
                if(!jsonData){
                    toast.error("Error occurred");
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getMedicalData();
    }, [medicalData, patient.patient_id, selectedDate]);

    useEffect(() => {
        const getECG = async () => {
            try {
                const response = await fetch(`http://localhost:5000/ecg/${patient.patient_id}/${selectedDate.getFullYear()}/${selectedDate.getMonth()+1}/${selectedDate.getDate()}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                setEcg(jsonData);
                if(!jsonData){
                    toast.error("Error occurred");
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getECG();
    }, [ecg, patient.patient_id, selectedDate]);

    useEffect(() => {
        const getHeartsounds = async () => {

            try {
                const response = await fetch(`http://localhost:5000/heartsound/${patient.patient_id}/${selectedDate.getFullYear()}/${selectedDate.getMonth()+1}/${selectedDate.getDate()}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                setHeartsounds(jsonData);

                if(!jsonData){
                    toast.error("Error occurred");
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getHeartsounds();
    }, [heartsounds, patient.patient_id, selectedDate]);

    useEffect(() => {
        const getBmiAnalytics = async () => {
            try {
                const response = await fetch(`http://localhost:5000/bmi/${patient.patient_id}/${selectedDate.getFullYear()}/${selectedDate.getMonth()+1}/${selectedDate.getDate()}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                setBmi(jsonData);

                if(!jsonData){
                    toast.error("Error occurred");
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getBmiAnalytics();
    }, [bmi, patient.patient_id, selectedDate]);

    useEffect(() => {
        const getHabitData = async () => {
            try {
                const response = await fetch(`http://localhost:5000/habit/${patient.patient_id}/${selectedDate.getFullYear()}/${selectedDate.getMonth()+1}/${selectedDate.getDate()}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                setHabitData(jsonData);

                if(!jsonData){
                    toast.error("Error occurred");
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getHabitData();

        const setHabitChart = () => {
            const sleep = []
            const diet = []
            const exercise = []
            const overall = []

            habitData.forEach(data => {
                let date = data.record_date.substr(0, 10);
                sleep.push({"x": date, "y": Math.abs(data.sleep_classification)});
                diet.push({"x": date, "y": Math.abs(data.diet_classification)});
                exercise.push({"x": date, "y": Math.abs(data.exercise_classification)});
                overall.push({"x": date, "y": Math.abs(data.overall_change)});
            })

            setHabitChartData({
                "sleep": sleep,
                "diet": diet,
                "exercise": exercise,
                "overall": overall
            })
        };
        setHabitChart();
    }, [habitData, patient.patient_id, selectedDate]);

    return (
        <Fragment>
            <Box marginY={3}>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography variant="h3" gutterBottom>Dashboard</Typography>
                    </Grid>
                    <Grid item>
                        <IconButton color="primary" onClick={handleDecreaseDate}>
                            <ArrowLeftIcon />
                        </IconButton>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                                disableToolbar
                                inputVariant="outlined"
                                format="MMMM d, yyyy"
                                value={selectedDate}
                                onChange={date => handleChangeDate(date)}
                                disableFuture
                                showTodayButton
                                required
                            />
                        </MuiPickersUtilsProvider>
                        <IconButton disabled={ disableNext } color="primary" onClick={handleIncreaseDate}>
                            <ArrowRightIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Box>
            <Box width="100%">
                <div className={classes.root}>
                    <Grid container spacing={3} className={classes.addPaddingBottom}>

                        {/* Patient's profile */}
                        <Grid item xs={12} sm={6}>
                            <Card variant="outlined" align="left" className={classes.cardContainer}>
                                <CardContent className={classes.whiteText}>
                                    <Grid container justify="space-between">
                                        <Grid item>
                                            <Typography variant="h4" gutterBottom>{patient.patient_name}'s Profile</Typography>
                                        </Grid>
                                        <Grid item>
                                            {allowEditDashboard ?
                                                <Button color="inherit" variant="outlined" size="small" onClick={handleOpenBMIDialog}>Update Height and Weight</Button> : ""}
                                        </Grid>
                                    </Grid>
                                    <Box marginTop={3}>
                                        <Grid container justify="space-evenly" alignItems="center">
                                            <Grid item xs={6}>
                                                <Chip className={classes.chip}
                                                      color="secondary"
                                                      variant="outlined"
                                                      size="medium"
                                                      icon={<FavoriteBorderIcon />}
                                                      label= { diseases[patient.disease-1] } />
                                                <ul>
                                                    <li><Typography variant="body2" >
                                                        {patient.patient_gender === "M" ? "Male" : "Female"},
                                                        { patient.patient_race === "M" ? " Malay" :
                                                            patient.patient_race === "C" ? " Chinese" :
                                                                patient.patient_race === "I" ? " Indian" :
                                                                    " Other" }
                                                    </Typography></li>
                                                    <li><Typography variant="body2" >
                                                        {new Date().getFullYear() - patient.patient_birth_year} years old
                                                    </Typography></li>
                                                    <li><Typography variant="body2">
                                                        { patient.patient_contact }
                                                    </Typography></li>
                                                    <li><Typography variant="body2">
                                                        { patient.patient_emgcy_no } (Emergency)
                                                    </Typography></li>
                                                    <li><Typography variant="body2">
                                                        { patient.is_married ? "Married" : "Single" }
                                                    </Typography></li>
                                                    <li><Typography variant="body2" >
                                                        { patient.is_vegan ? "Vegetarian" : "Non-vegetarian" }
                                                    </Typography></li>
                                                    <li>
                                                        <Typography variant="body2" >
                                                            {bmi.height_in_m} m, {bmi.weight_in_kg} kg
                                                        </Typography>
                                                    </li>
                                                </ul>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <Box marginTop={1}>
                                                    <Paper className={classes.changeResult2} elevation={3} variant="outlined">
                                                        <Typography variant="body1" align="center">
                                                            BMI: {Math.round(bmi.current_bmi * 100) / 100} ({bmi.current_category})
                                                        </Typography>
                                                    </Paper>
                                                    <Paper className={classes.changeResult} elevation={3} variant="outlined">
                                                        <Typography variant="caption">{bmi.analytics}</Typography>
                                                    </Paper>
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* Overall habit */}
                        <Grid item xs={12} sm={6}>
                            <Card variant="outlined" align="left" className={classes.whiteDashboard}>
                                <CardContent>
                                    <Typography variant="h4" gutterBottom>Overall Habit</Typography>
                                    <Typography variant="caption" gutterBottom>
                                        The patient's overall habit-change over the most recent 7 days.
                                        <br/>Click each point to download the habit data in csv format.
                                    </Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={8}>
                                            <Box marginTop={5} height="220px">
                                                <ResponsiveLine
                                                    data={[
                                                        {
                                                            "id": "Overall",
                                                            "data": habitChartData.overall
                                                        }
                                                    ]}
                                                    margin={{ top: 10, right: 10, bottom: 60, left: 60 }}
                                                    xScale={{ type: 'point' }}
                                                    yScale={{ type: 'linear', min: '0', max: '4', stacked: false, reverse: false }}
                                                    yFormat="d"
                                                    axisTop={null}
                                                    axisRight={null}
                                                    axisBottom={{
                                                        orient: 'bottom',
                                                        tickSize: 5,
                                                        tickPadding: 5,
                                                        tickRotation: -45,
                                                        legend: ''
                                                    }}
                                                    axisLeft={{
                                                        tickValues: [0, 1, 2, 3, 4],
                                                        orient: 'left',
                                                        tickPadding: 5,
                                                        tickRotation: 0,
                                                        legend: 'Level of changes',
                                                        legendOffset: -40,
                                                        legendPosition: 'middle'
                                                    }}
                                                    pointSize={8}
                                                    pointColor={{ theme: 'background' }}
                                                    pointBorderWidth={2}
                                                    pointBorderColor={{ from: 'serieColor' }}
                                                    pointLabelYOffset={-12}
                                                    colors={{ "scheme": "pastel1" }}
                                                    useMesh={true}
                                                    enableGridX={false}
                                                    onClick={(data, event) => {
                                                        handleOpenDownloadDialog(data.data.x)
                                                    }}
                                                />
                                            </Box>
                                        </Grid>
                                        <Grid item xs={4}>
                                            <Box marginTop={3}>
                                                <Typography variant="h6" align="center">Level of changes of today compared with yesterday:</Typography>
                                                <Box marginTop={3}>
                                                    <Paper className={classes.changeResult2} elevation={3} variant="outlined">
                                                        <Typography variant="body1" align="center">
                                                            { habitData.length === 0 ? "No Data"
                                                                : habitData[habitData.length-1].habit_analytics.overall_cat }
                                                        </Typography>
                                                        <Typography variant="caption" align="center">
                                                            {habitData.length === 0 ? "No Data"
                                                                : habitData[habitData.length-1].habit_analytics.overall_analytics }
                                                        </Typography>
                                                    </Paper>
                                                </Box>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* Habits - Diet, Exercise and Sleep */}
                        <Grid item xs={12}>
                            <Card variant="outlined" align="left" className={classes.secondaryBackground}>
                                <CardContent className={classes.whiteText}>
                                    <Grid container justify="space-between">
                                        <Grid item>
                                            <Typography variant="h4" gutterBottom>Diet, Exercise and Sleep Habit</Typography>
                                            <Typography variant="caption" gutterBottom>The patient's diet, exercise and sleep habit-change over the most recent 7 days. </Typography>
                                        </Grid>
                                        <Grid item>
                                            {allowEditDashboard ?
                                                <Button color="inherit" variant="outlined" size="small"
                                                        onClick={()=>handleOpenHabitDialog()}>Upload Data</Button> : ""}
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs={9}>
                                            <Box marginTop={5} height="300px">
                                                <ResponsiveLine
                                                    data={[
                                                        {
                                                            "id": "Diet",
                                                            "data": habitChartData.diet
                                                        },
                                                        {
                                                            "id": "Exercise",
                                                            "data": habitChartData.exercise
                                                        },
                                                        {
                                                            "id": "Sleep",
                                                            "data": habitChartData.sleep
                                                        }
                                                    ]}
                                                    margin={{ top: 10, right: 110, bottom: 60, left: 60 }}
                                                    xScale={{ type: 'point' }}
                                                    yScale={{ type: 'linear', min: '0', max: '5', stacked: false, reverse: false }}
                                                    yFormat="d"
                                                    axisTop={null}
                                                    axisRight={null}
                                                    axisBottom={{
                                                        orient: 'bottom',
                                                        tickSize: 5,
                                                        tickPadding: 5,
                                                        tickRotation: -45,
                                                        legend: ''
                                                    }}
                                                    axisLeft={{
                                                        tickValues: [0, 1, 2, 3, 4, 5],
                                                        orient: 'left',
                                                        tickPadding: 5,
                                                        tickRotation: 0,
                                                        legend: 'Level of healthy / effort',
                                                        legendOffset: -40,
                                                        legendPosition: 'middle'
                                                    }}
                                                    pointSize={8}
                                                    pointColor={{ theme: 'background' }}
                                                    pointBorderWidth={2}
                                                    pointBorderColor={{ from: 'serieColor' }}
                                                    pointLabelYOffset={-12}
                                                    legends={[
                                                        {
                                                            anchor: 'bottom-right',
                                                            direction: 'column',
                                                            justify: false,
                                                            translateX: 100,
                                                            translateY: 0,
                                                            itemsSpacing: 0,
                                                            itemDirection: 'left-to-right',
                                                            itemWidth: 80,
                                                            itemHeight: 20,
                                                            itemOpacity: 1,
                                                            symbolSize: 12,
                                                            symbolShape: 'circle',
                                                            symbolBorderColor: 'rgba(0, 0, 0, .5)'
                                                        }
                                                    ]}
                                                    theme = {{
                                                        textColor: "#ffffff",
                                                        axis: {
                                                            ticks: {
                                                                line: {
                                                                    stroke: "#ffffff"
                                                                }
                                                            },
                                                        },
                                                        tooltip: {
                                                            container: {
                                                                background: '#C6C6C6',
                                                            },
                                                        },
                                                    }}
                                                    colors={{ "scheme": "set3" }}
                                                    useMesh={true}
                                                    enableGridX={false}
                                                /></Box>
                                        </Grid>
                                        <Grid item xs={3}>
                                            <Typography variant="h6" align="center">Level of changes of today compared with yesterday:</Typography>
                                            <Box marginTop={1}>
                                                <Paper className={classes.changeResult} elevation={3} variant="outlined">
                                                    <Typography variant="body1" align="center">
                                                        Diet: {habitData.length === 0 ? "No Data"
                                                        : habitData[habitData.length-1].habit_analytics.diet_cat }
                                                    </Typography>
                                                    <Typography variant="caption" align="center">
                                                        {habitData.length === 0 ? "No Data"
                                                            : habitData[habitData.length-1].habit_analytics.diet_analytics }
                                                    </Typography>
                                                </Paper>
                                                <Paper className={classes.changeResult} elevation={3} variant="outlined">
                                                    <Typography variant="body1" align="center">
                                                        Exercise: {habitData.length === 0 ? "No Data"
                                                        : habitData[habitData.length-1].habit_analytics.exercise_cat }
                                                    </Typography>
                                                    <Typography variant="caption">
                                                        {habitData.length === 0 ? "No Data"
                                                            : habitData[habitData.length-1].habit_analytics.exercise_analytics }
                                                    </Typography>
                                                </Paper>
                                                <Paper className={classes.changeResult} elevation={3} variant="outlined">
                                                    <Typography variant="body1" align="center">
                                                        Sleep: {habitData.length === 0 ? "No Data"
                                                        : habitData[habitData.length-1].habit_analytics.sleep_cat }
                                                    </Typography>
                                                    <Typography variant="caption" align="center">
                                                        {habitData.length === 0 ? "No Data"
                                                            : habitData[habitData.length-1].habit_analytics.sleep_analytics }
                                                    </Typography>
                                                </Paper>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* ECG */}
                        <Grid item xs={12} sm={6}>
                            <Card variant="outlined" align="left" className={classes.secondaryBackground}>
                                <CardContent className={classes.whiteText}>
                                    <Grid container justify="space-between">
                                        <Grid item>
                                            <Typography variant="h5" gutterBottom>Stress Classification Based On ECG</Typography>
                                            <Typography variant="caption" gutterBottom>The patient's stress level over the most recent 7 days based on ECG. </Typography>
                                        </Grid>
                                        <Grid item>
                                            {allowEditDashboard ?
                                                <Button color="inherit" variant="outlined" size="small" onClick={handleOpenEcgDialog}>Upload Data</Button> : ""}
                                        </Grid>
                                    </Grid>

                                    {ecg.length !== 0 ?
                                        <Box marginTop={5} height="200px">
                                            <Box marginBottom={3}>
                                                <Grid container justify="space-evenly">
                                                    <Grid item>
                                                        <Square classes={classes} color={"#ffffcb"}/>
                                                        <Typography variant="caption">No Data</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Square classes={classes} color={"#fd8c3c"}/>
                                                        <Typography variant="caption">Normal</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Square classes={classes} color={"#810026"}/>
                                                        <Typography variant="caption">Stress</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Box>
                                            <ResponsiveHeatMap
                                                data = {
                                                    [{
                                                        "date": "Class",
                                                        [moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).format('YYYY-MM-DD')]: getEcgClass(moment(selectedDate).format('YYYY-MM-DD')),
                                                    }]
                                                }
                                                keys = { [moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).format('YYYY-MM-DD')
                                                ]}
                                                minValue={0}
                                                maxValue={2}
                                                indexBy="date"
                                                margin={{ top: 0, right: 10, bottom: 0, left: 10 }}
                                                forceSquare={true}
                                                axisTop={{ orient: 'top', tickSize: 5, tickPadding: 5, tickRotation: -45, legend: '', legendOffset: 36 }}
                                                axisRight={null}
                                                axisBottom={null}
                                                axisLeft={null}
                                                cellOpacity={1}
                                                cellBorderWidth={1}
                                                cellBorderColor={ "#FFFFFF" }
                                                enableLabels={false}
                                                defs={[
                                                    {
                                                        id: 'lines',
                                                        type: 'patternLines',
                                                        background: 'inherit',
                                                        color: 'rgba(0, 0, 0, 0.1)',
                                                        rotation: -45,
                                                        lineWidth: 4,
                                                        spacing: 7
                                                    }
                                                ]}
                                                fill={[ { id: 'lines' } ]}
                                                animate={true}
                                                motionConfig="gentle"
                                                motionStiffness={80}
                                                motionDamping={9}
                                                hoverTarget="cell"
                                                cellHoverOthersOpacity={0.25}
                                                theme = {{
                                                    textColor: "#ffffff",
                                                    axis: {
                                                        ticks: {
                                                            line: {
                                                                stroke: "#ffffff"
                                                            }
                                                        },
                                                    },
                                                }}
                                                colors={"YlOrRd"}
                                                tooltip={({ xKey, value }) => (
                                                    <span style={{ color: "#000000" }}>
                                                    {xKey}: <strong>{value === 0 ? "No Data"
                                                        : value === 1 ? "Normal" : "Stress"}</strong>
                                                </span>
                                                )}
                                            />
                                        </Box> : ""}

                                    <Box marginTop="50px">
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <Typography variant="body1">Date</Typography>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <Typography variant="body1">Automatically classified as</Typography>
                                            </Grid>
                                        </Grid>
                                        <Divider className={classes.divider}/>
                                        {ecg.length !== 0 ?

                                            ecg.map((e) => (
                                                <Box marginY={2}>
                                                    <Grid container justify="space-between" alignItems="center">
                                                        <Grid item xs={4}>
                                                            <Typography variant="body2">{e.record_datetime.substr(0, 10)}</Typography>
                                                        </Grid>
                                                        <Grid item xs={3}>
                                                            {e.is_stressed ?
                                                                <Chip variant="outlined" className={classes.stress} label= "Stress"/>
                                                                : <Chip variant="outlined" className={classes.normal} label= "Normal"/> }
                                                        </Grid>
                                                        <Grid item xs={5}>
                                                            <Grid container justify="space-around">
                                                                <Grid item>
                                                                    <Button color="inherit" variant="outlined" size="small"
                                                                            onClick={()=>handleOpenSignalDialog(e.csv_filename, e.csv_filepath)}>
                                                                        Show ECG
                                                                    </Button>
                                                                </Grid>
                                                                {currentUser.user_role === "N" ?
                                                                    <Grid item>
                                                                        <Button color="inherit" size="small"
                                                                                onClick={() => deleteEcg(e.ecg_id)}>
                                                                            <DeleteForeverIcon/>
                                                                        </Button>
                                                                    </Grid>
                                                                    : ""
                                                                }
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Box>
                                            )) : <Typography variant="body2">No record.</Typography>}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* heartsound */}
                        <Grid item xs={12} sm={6}>
                            <Card variant="outlined" align="left" className={classes.primaryBackground}>
                                <CardContent className={classes.whiteText}>
                                    <Grid container justify="space-between">
                                        <Grid item>
                                            <Typography variant="h5" gutterBottom>Heartbeat Sound Classification</Typography>
                                            <Typography variant="caption" gutterBottom>The patient's heartbeat sound types over the most recent 7 days. </Typography>
                                        </Grid>
                                        <Grid item>
                                            {allowEditDashboard ?
                                                <Button color="inherit" variant="outlined" size="small" onClick={handleOpenHeartDialog}>Upload Data</Button> : ""}
                                        </Grid>
                                    </Grid>

                                    {heartsounds.length !== 0 ?
                                        <Box marginTop={5} height="200px">
                                            <Box marginBottom={3}>
                                                <Grid container justify="space-evenly">
                                                    <Grid item>
                                                        <Square classes={classes} color={"#ffffd9"}/>
                                                        <Typography variant="caption">No Data</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Square classes={classes} color={"#7fcebb"}/>
                                                        <Typography variant="caption">Normal</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Square classes={classes} color={"#225ea7"}/>
                                                        <Typography variant="caption">Murmur</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Square classes={classes} color={"#081c57"}/>
                                                        <Typography variant="caption">Extrasystole</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Box>
                                            <ResponsiveHeatMap
                                                data = {
                                                    [{
                                                        "date": "Class",
                                                        [moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD')),
                                                        [moment(selectedDate).format('YYYY-MM-DD')]: getHeartSoundClass(moment(selectedDate).format('YYYY-MM-DD')),
                                                    }]
                                                }
                                                keys = { [moment(selectedDate).subtract(6, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(5, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(4, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(3, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(2, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).subtract(1, 'days').format('YYYY-MM-DD'),
                                                    moment(selectedDate).format('YYYY-MM-DD')
                                                ]}
                                                minValue={0}
                                                maxValue={3}
                                                indexBy="date"
                                                margin={{ top: 0, right: 10, bottom: 0, left: 10 }}
                                                forceSquare={true}
                                                axisTop={{ orient: 'top', tickSize: 5, tickPadding: 5, tickRotation: -45, legend: '', legendOffset: 36 }}
                                                axisRight={null}
                                                axisBottom={null}
                                                axisLeft={null}
                                                cellOpacity={1}
                                                cellBorderWidth={1}
                                                cellBorderColor={ "#FFFFFF" }
                                                enableLabels={false}
                                                defs={[
                                                    {
                                                        id: 'lines',
                                                        type: 'patternLines',
                                                        background: 'inherit',
                                                        color: 'rgba(0, 0, 0, 0.1)',
                                                        rotation: -45,
                                                        lineWidth: 4,
                                                        spacing: 7
                                                    }
                                                ]}
                                                fill={[ { id: 'lines' } ]}
                                                animate={true}
                                                motionConfig="gentle"
                                                motionStiffness={80}
                                                motionDamping={9}
                                                hoverTarget="cell"
                                                cellHoverOthersOpacity={0.25}
                                                theme = {{
                                                    axis: {
                                                        textColor: "#ffffff",
                                                        ticks: {
                                                            line: {
                                                                stroke: "#ffffff"
                                                            },
                                                            text: {
                                                                fill: "#ffffff",
                                                            },
                                                        },
                                                    },
                                                }}
                                                colors={"YlGnBu"}
                                                tooltip={({ xKey, value }) => (
                                                    <span style={{ color: "#000000" }}>
                                                        {xKey}: <strong>{value === 0 ? "No Data"
                                                        : value === 1 ? "Normal"
                                                            : value === 2 ? "Murmur" : "Extrasystole"}</strong>
                                                    </span>
                                                )}
                                            /></Box> : ""}

                                    <Box marginTop={3}>
                                        <Grid container>
                                            <Grid item xs={2}>
                                                <Typography variant="body1">Date</Typography>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Typography variant="body1">Audio</Typography>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <Typography variant="body1">Automatically classified as</Typography>
                                            </Grid>
                                        </Grid>
                                        <Divider className={classes.divider}/>
                                        {heartsounds.length !== 0 ?
                                            heartsounds.map((heartsound) => (
                                                <Box marginY={2}>
                                                    <Grid container justify="space-between" alignItems="center">
                                                        <Grid item xs={2}>
                                                            {heartsound.record_datetime.substr(0, 10)}
                                                        </Grid>

                                                        <Grid item xs={6}>
                                                            <audio controls style={{maxWidth: 210}}>
                                                                <source src={heartsound.heartsound_path} type="audio/wav"/>
                                                                Your browser does not support the audio element.
                                                            </audio>
                                                        </Grid>
                                                        <Grid item xs={3}>
                                                            {heartsound.heartsound_class === "Normal" ?
                                                                <Chip variant="outlined" className={classes.normalHeart} label="Normal"/>
                                                                : heartsound.heartsound_class === "Murmur" ?
                                                                    <Chip variant="outlined" className={classes.murmur} label="Murmur"/>
                                                                    : <Chip variant="outlined" className={classes.extra} label="Extrasystole"/>}
                                                        </Grid>
                                                        <Grid item xs={1}>
                                                            {currentUser.user_role === "N" ?
                                                                <Button color="inherit" size="small" onClick={() => deleteHeartsound(heartsound.heartbeat_sound_id)}><DeleteForeverIcon/></Button>
                                                                : ""
                                                            }
                                                        </Grid>
                                                    </Grid>
                                                </Box>
                                            )) : <Typography variant="body2">No record.</Typography>}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* medical data */}
                        <Grid item xs={12}>
                            <Card variant="outlined" align="left" className={classes.whiteDashboard}>
                                <CardContent>
                                    <Typography variant="h4" gutterBottom>Medical Data</Typography>
                                    <Typography variant="caption" gutterBottom>Medical data of the most recent 7 days. </Typography>
                                    <TableContainer>
                                        <Table className={classes.table} aria-label="medical data">
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>Date</TableCell>
                                                    <TableCell>Blood Pressure and Level</TableCell>
                                                    <TableCell>Blood Glucose and Level</TableCell>
                                                    <TableCell>Wearing Duration and Rate</TableCell>
                                                    <TableCell>Heart Rate (bpm)</TableCell>
                                                    <TableCell>Daily Steps</TableCell>
                                                    <TableCell>Daily Activity</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>

                                                {medicalData.length !== 0 ?
                                                    medicalData.map((data) => (
                                                        <TableRow key={data.record_date}>
                                                            <TableCell component="th" scope="row">{data.record_date.substr(0, 10)}</TableCell>
                                                            <TableCell>{data.systolic_pressure}/{data.diastolic_pressure} ({data.blood_pressure_lvl})</TableCell>
                                                            <TableCell>{data.blood_glucose} ({data.blood_glucose_lvl})</TableCell>
                                                            <TableCell>{data.duration_wearing_min} min ({data.wearing_rate}%)</TableCell>
                                                            <TableCell>
                                                                <ul className={classes.ul}>
                                                                    <li>Min: {data.min_heartbeat}</li>
                                                                    <li>Avg: {data.avg_heartbeat}</li>
                                                                    <li>Max: {data.max_heartbeat}</li>
                                                                </ul>
                                                            </TableCell>
                                                            <TableCell style={{ width: "max-content" }}>
                                                                <ul className={classes.ul}>
                                                                    <li>{data.daily_steps} steps</li>
                                                                    <li>{data.step_distance} km</li>
                                                                    <li>{data.step_calories} kcal</li>
                                                                </ul>

                                                            </TableCell>
                                                            <TableCell style={{ width: "max-content" }}>
                                                                <ul className={classes.ul}>
                                                                    <li>{data.daily_activity_time_hr} hour(s)</li>
                                                                    <li>{data.daily_activity_distance} km</li>
                                                                    <li>{data.daily_activity_calories} kcal</li>
                                                                </ul>
                                                            </TableCell>
                                                        </TableRow>
                                                    )) :
                                                    <TableRow>
                                                        <TableCell>
                                                            <Typography variant="body2">No record.</Typography>
                                                        </TableCell>
                                                    </TableRow>}

                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </Box>

            <Dialog open={openECG} onClose={handleCloseEcgDialog} className={classes.dialog}>
                <Typography variant="h4">
                    <DialogTitle disableTypography>Upload New ECG Data</DialogTitle>
                </Typography>
                <Typography variant="caption" gutterBottom>
                    <DialogTitle disableTypography>Accept .csv format only.</DialogTitle>
                </Typography>

                <DialogContent>
                    <FileUpload patient_id={patient.patient_id} type="ecg" />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEcgDialog} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openHeart} onClose={handleCloseHeartDialog} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Upload New Heartbeat Sound</DialogTitle>
                </Typography>
                <Typography variant="caption" gutterBottom>
                    <DialogTitle disableTypography>Accept .wav format only.</DialogTitle>
                </Typography>

                <DialogContent>
                    <FileUpload patient_id={patient.patient_id} handleCloseDialog={handleCloseHeartDialog} type={"heartsound"}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseHeartDialog} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openHabit} onClose={handleCloseHabitDialog} className={classes.dialog}>
                <Typography variant="h4">
                    <DialogTitle disableTypography>Upload New Habit Data</DialogTitle>
                </Typography>
                <Typography variant="caption" gutterBottom>
                    <DialogTitle disableTypography>Accept .csv format only.</DialogTitle>
                </Typography>

                <DialogContent>
                    <FileUpload patient_id={patient.patient_id} type="habit" />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseHabitDialog} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openBMI} onClose={handleCloseBMIDialog} className={classes.dialog}>
                <Typography variant="h4">
                    <DialogTitle disableTypography>Update New Height and Weight</DialogTitle>
                </Typography>

                <DialogContent>
                    <form>
                        <TextField
                            className={classes.inputFieldHalf}
                            type="number"
                            variant="outlined"
                            label="Patient Height"
                            value={patientHeightInM}
                            onChange={handleHeightChange}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">m</InputAdornment>,
                                inputProps: {min: 0, max: 2.5, step: 0.10}
                            }}
                            required
                            error={heightErrorText !== ""}
                            helperText={heightErrorText}/>
                        <TextField
                            className={classes.inputFieldHalf}
                            type="number"
                            variant="outlined"
                            label="Patient Weight"
                            value={patientWeightInKg}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">kg</InputAdornment>,
                                inputProps: { min: 0, max: 300, step:0.5 }
                            }}
                            onChange={handleWeightChange}
                            required
                            error={weightErrorText !== ""}
                            helperText={weightErrorText}/>

                    </form>
                    <Button onClick={onSubmitBmiForm} color="primary">Save</Button>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseBMIDialog} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openSignal} onClose={handleCloseSignalDialog} className={classes.dialog} maxWidth="xl" fullWidth>
                <Typography variant="h4">
                    <DialogTitle disableTypography>ECG Signal Diagram</DialogTitle>
                </Typography>
                <DialogContent>
                    <EcgSignalGraph filename={ currentFilename } filepath={ currentFilepath }/>
                </DialogContent>
            </Dialog>

            <Dialog open={openDownload} onClose={handleCloseDownloadDialog} className={classes.dialog}>
                <Typography variant="h4">
                    <DialogTitle disableTypography>Download Habit Data</DialogTitle>
                </Typography>

                <DialogContent>
                    <Download filepath={downloadPath} date={downloadDate} />
                </DialogContent>
            </Dialog>
        </Fragment>
    );
}

export default Dashboard;