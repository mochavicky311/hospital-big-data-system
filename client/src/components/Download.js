import React, {Fragment} from "react";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";


const Download = ({filepath, date}) => {

    return (
        <Fragment>
            <Typography variant="caption" gutterBottom>
                Date at {date}
            </Typography>

            <Box marginY={2}>
                <Link href={filepath}>Download</Link>
            </Box>
        </Fragment>
    );

}

export default Download;