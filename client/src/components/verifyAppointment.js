import {toast} from "react-toastify";
import Moment from "moment";
import { extendMoment } from "moment-range";

const moment = extendMoment(Moment);


export const verifyTitle = (title) => {
    if(title === undefined || title.trim() === ''){
        toast.error('The title cannot be empty.');
        return false;
    }
    return true;
}

export const verifySlotsDT = async (startDate, endDate, appId, doctor, patient) => {
    startDate = new Date(startDate);
    endDate = new Date(endDate);

    const appointments = await getAppointments(startDate, doctor, patient);

    if(!verifyTime(startDate, endDate)){
        toast.error("Not available time slot.");
        return false;
    }
    if(!verifyWeekend(startDate) || !verifyWeekend(endDate)) {
        toast.error("Weekend not available.");
        return false;
    }
    if(!verifyOverlap(startDate, endDate, appId, appointments)){
        toast.error("Overlap with other appointment.");
        return false;
    }
    if(!verifyPrevious(startDate) || !verifyPrevious(endDate)){
        toast.error("Cannot select previous date time.");
        return false;
    }

    return true;
}


const verifyTime = (startDate, endDate) => {
    /*
    Not available time slots
    1. 1pm - 2pm
    2. before 9am
    3. after 5pm
    */

    let startHour = startDate.getHours();
    let endHour = endDate.getHours();
    let endMin = endDate.getMinutes();

    let validStart = true;
    let validEnd = true;

    if (startHour<9 || startHour>=17 || startHour===13) validStart = false;

    if (endHour<=9 || endHour>17 || endHour===14 || (endHour===17 && endMin>0) || (endHour===13 && endMin>0))
        validEnd = false;

    return validStart && validEnd && !(startHour<=13 && endHour>14)
}


const verifyWeekend = (date) => {
    return (date.getDay() !== 0 && date.getDay() !== 6);
}


const verifyOverlap = (startDate, endDate, appId, appointments) => {
    let newRange = moment.range(moment(startDate), moment(endDate));

    if(appointments.length > 0){
        const existingApp = appointments.filter(appointment => appointment.id.toString() !== appId.toString());

        for(let i=0; i<existingApp.length; i++) {
            let compareRange = moment.range(
                moment(new Date(existingApp[i].startDate)),
                moment(new Date(existingApp[i].endDate))
            );

            if(newRange.overlaps(compareRange)){
                return false;
            }
        }
    } else {
        return true;
    }
    return true;
}

const verifyPrevious = (date) => {
    return date >= new Date();
}


const getAppointments = async (startDate, doctor, patient) => {

    const year = startDate.getFullYear();
    const month = startDate.getMonth()+1;
    const date = startDate.getDate();

    let appointments = [];

    try {
        const response = await fetch(`http://localhost:5000/appointment/d1/${year}/${month}/${date}/${doctor}`, {
            method: "GET",
            headers: {token: localStorage.token}
        });
        const jsonData = await response.json();

        if (!jsonData) {
            toast.error("Error occurred");
        }

        appointments = jsonData;

    } catch (err) {
        console.error(err.message);
    }

    try {
        const response = await fetch(`http://localhost:5000/appointment/p1/${year}/${month}/${date}/${patient}`, {
            method: "GET",
            headers: {token: localStorage.token}
        });
        const jsonData = await response.json();

        if (!jsonData) {
            toast.error("Error occurred");
        }

        appointments = appointments.concat(jsonData);

    } catch (err) {
        console.error(err.message);
    }

    return appointments;
}