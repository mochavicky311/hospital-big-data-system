import React, {Fragment, useContext, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Paper from '@material-ui/core/Paper';
import {EditingState, IntegratedEditing, ViewState} from '@devexpress/dx-react-scheduler';
import {
    AppointmentForm,
    Appointments,
    AppointmentTooltip,
    ConfirmationDialog,
    DateNavigator,
    DragDropProvider,
    Resources,
    Scheduler,
    TodayButton,
    Toolbar,
    WeekView,
    CurrentTimeIndicator,
} from '@devexpress/dx-react-scheduler-material-ui';
import {verifySlotsDT, verifyTitle} from "./verifyAppointment";
import {UserContext} from "../UserContext";
import {
    createNewAppointments,
    getAppointments,
    deleteAppointment,
    modifyAppointment,
    getPatientsUnderDoctor, getMedName
} from "../api/Api";


toast.configure();


const Appointment = ({ classes, user_info }) => {
    const currentUser = useContext(UserContext);
    const [ selectedDate, setSelectedDate ] = useState(new Date());
    const [ doctors, setDoctors ] = useState([]);
    const [ patients, setPatients ] = useState([]);
    const [ appointments, setAppointments ] = useState([]);
    const mainResourceName ='extraField';
    const resources = [
        {
            fieldName: 'doctor',
            title: 'Doctor',
            instances: doctors
        },
        {
            fieldName: 'patient',
            title: 'Patient',
            instances: patients
        },
    ];
    const messages = {
        moreInformationLabel: 'Description',
    };


    const currentDateChange = currentDate => {
        setSelectedDate(currentDate);
    };


    const BooleanEditor = (props) => {
        if (props.type === 'booleanEditor') {
            return null;
        }
        return null;
    }

    const commitChanges = async ({ added, changed, deleted }) => {

        let data = appointments;

        if (added) {
            let validAddedTitle = true;
            let validAddedDT = true;

            if (added.title === undefined || added.title.trim() === ''
                || added.doctor === undefined || added.patient === undefined) {
                // verify whether title, doctor and patient is not null
                toast.error('The title, doctor and patient cannot be empty.');
                validAddedTitle = false;
            }

            if (!await verifySlotsDT(added.startDate, added.endDate, -99, added.doctor, added.patient)){
                // verify datetime
                validAddedDT = false;
            }

            if(validAddedTitle && validAddedDT) {
                // create new appointment
                await createAppointments(added);

                window.location.reload();
            }
        }

        if (changed) {
            if (await isValidChange(changed)) {
                data = data.map(appointment => (
                    changed[appointment.id] ? {...appointment, ...changed[appointment.id]} : appointment));

                for(let appId of Object.keys(changed)){
                    await modifyAppointments(appId, changed[appId]);
                }
            }
        }

        if (deleted !== undefined) {
            data = data.filter(appointment => appointment.id !== deleted);
            await deleteAppointments(deleted);
        }

        setAppointments(data);
    }

    const TimeTableCell = props => {

        const { startDate } = props;
        const date = new Date(startDate);

        // cannot create appointment on weekends
        if (date.getDay() === 0 || date.getDay() === 6) {
            return <WeekView.TimeTableCell
                {...props}
                onDoubleClick={() =>
                    toast.error("Sorry, this slot is not available.")
                }
                className={classes.disableCell} />;
        }

        // cannot create appointment on lunch break.
        if (date.getHours() === 13) {
            return <WeekView.TimeTableCell
                {...props}
                onDoubleClick={() =>
                    toast.error("Sorry, this slot is not available.")
                }
                className={classes.disableCell} />;
        }

        // disable create appointment of past date and time.
        if (date < new Date()) {
            return <WeekView.TimeTableCell
                {...props}
                onDoubleClick={() =>
                    toast.error("Sorry, this slot is not available.")
                }
                className={classes.disableCell} />;
        }

        // highlight today column
        if (date.getDate() === new Date().getDate()) {
            return <WeekView.TimeTableCell {...props} className={classes.todayCell}/>;
        } return <WeekView.TimeTableCell {...props} />;
    };

    const DayScaleCell = props => {
        const { today } = props;

        if (today) {
            return <WeekView.DayScaleCell {...props} className={classes.today} />;
        } return <WeekView.DayScaleCell {...props} />;
    };

    const Appointment = ({children, style, ...restProps}) => (
        <Appointments.Appointment
            {...restProps}
            style={{
                ...style,
                backgroundColor: '#f76c6c',
                borderRadius: '8px',
                width: '108%'
            }}
        >
            {children}
        </Appointments.Appointment>
    );


    const isValidChange = async (changed) => {

        let titleChanged;
        let startDateChanged;
        let endDateChanged;
        let patientChanged;

        const appId = Object.keys(changed)[0];
        const changedAppointment = changed[appId];
        let oriAppointment = {};
        for (let i = 0; i < appointments.length; i++) {
            if (appointments[i].id.toString() === appId.toString()) {
                oriAppointment = {...appointments[i]};
            }
        }

        // check if certain keys are modified
        const keys = Object.keys(changedAppointment)
        titleChanged = keys.includes("title");
        startDateChanged = keys.includes("startDate");
        endDateChanged = keys.includes("endDate");
        patientChanged = keys.includes("patients");

        // update current doctor and patient
        let currentDoc = oriAppointment.doctor;
        let currentPat;
        if (patientChanged) {
            currentPat = changedAppointment.patient;
        } else {
            currentPat = oriAppointment.patient;
        }

        // initialize start and end dates
        let currentStart = new Date(oriAppointment.startDate);
        let currentEnd = new Date(oriAppointment.endDate);

        // verify title
        if (titleChanged) {
            if (!verifyTitle(changedAppointment.title))
                return false;
        }

        // update dates
        if (startDateChanged && endDateChanged) {
            currentStart = changedAppointment.startDate;
            currentEnd = changedAppointment.endDate;
        } else if (startDateChanged && !endDateChanged) {
            currentStart = changedAppointment.startDate;
        } else if (!startDateChanged && endDateChanged) {
            currentEnd = changedAppointment.endDate;
        } else {
            return true;
        }

        // verify dates
        return await verifySlotsDT(currentStart, currentEnd, appId, currentDoc, currentPat);
    }


    const createAppointments = async (newApp) => {
        return await createNewAppointments(newApp);
    }


    const deleteAppointments = async (id) => {

        const parseRes = await deleteAppointment(id);

        if(parseRes === "Deleted successfully!"){
            toast.success(parseRes);
        } else {
            toast.error(parseRes);
        }

    }


    const modifyAppointments = async (appId, modifiedColumns) => {

        const parseRes = await modifyAppointment(appId, modifiedColumns);

        if(parseRes==="Updated successfully!"){
            toast.success(parseRes);
        } else {
            toast.error(parseRes);
        }

    }


    useEffect(() => {
        const year = selectedDate.getFullYear();
        const month = selectedDate.getMonth()+1;
        const date = selectedDate.getDate();

        const getPatients = async () => {

            const jsonData = await getPatientsUnderDoctor(currentUser.user_id);

            if (!jsonData) {
                toast.error("Error occurred");
            }

            return jsonData;

        }

        const getDoctorName = async med_id => {

            const jsonData = await getMedName(med_id);

            if(!jsonData){
                toast.error("Error occurred");
            }

            return jsonData.med_name;

        }

        const getAllAppointments = async (year, month, day, id, c) => {
            try {

                const jsonData = await getAppointments(year, month, day, id, c);

                if (!jsonData) {
                    toast.error("Error occurred");
                }

                return jsonData;

            } catch (err) {
                console.error(err.message);
            }
        }

        // for doctor, get list of patients
        if(currentUser.user_role === 'D'){

            getAllAppointments(year, month, date, currentUser.user_id, 'd').then(res => setAppointments(res));

            setDoctors([{
                id: currentUser.user_id, text: `Dr ${user_info.name}`
            }])

            getPatients().then(res => {
                const p = []
                res.forEach(patient => {
                    p.push({id: patient.patient_id, text: patient.patient_name})
                })
                setPatients(p);
            });

        } else {
            getAllAppointments(year, month, date, currentUser.user_id, 'p').then(res => setAppointments(res));

            getDoctorName(user_info.med_id).then(res => {
                setDoctors([{
                    id: user_info.med_id,
                    text: `Dr ${res}`
                }])
            })

            setPatients([{
                id: currentUser.user_id, text: user_info.patient_name
            }])
        }

    }, [user_info, selectedDate, currentUser.user_id, currentUser.user_role])


    return (
        <Fragment>
            <Box marginY={3}>
                <Typography variant="h3">Appointments</Typography>
                <div className={classes.root}>
                    <Box marginY={3}>
                        <Card className={classes.whiteDashboard}>
                            <CardContent>
                                <Typography variant="caption" gutterBottom>
                                    Available appointment time slots:
                                    <strong> Mon to Fri: 9am - 1pm, 2pm - 5pm</strong><br/>
                                    Double-click on any available slot to create new appointment.
                                </Typography>

                                <Box marginY={3}>
                                    <Paper>
                                        <Scheduler
                                            data={appointments} locale="en-sg">
                                            <ViewState
                                                currentDate={selectedDate}
                                                onCurrentDateChange={date => currentDateChange(date)}
                                                currentViewName="week" />
                                            <EditingState
                                                onCommitChanges={(changeSet) => commitChanges(changeSet)}
                                            />
                                            <IntegratedEditing />
                                            <ConfirmationDialog />
                                            <WeekView
                                                name="week"
                                                displayName="Week"
                                                cellDuration={60}
                                                startDayHour={9}
                                                endDayHour={17}
                                                timeTableCellComponent={TimeTableCell}
                                                dayScaleCellComponent={DayScaleCell}
                                            />
                                            <Toolbar />
                                            <DateNavigator />
                                            <TodayButton />
                                            <Appointments appointmentComponent={Appointment}/>
                                            <AppointmentTooltip showOpenButton showDeleteButton/>
                                            <AppointmentForm
                                                booleanEditorComponent={BooleanEditor}
                                                messages={messages}/>
                                            <Resources
                                                data={ resources }
                                                mainResourceName={ mainResourceName }
                                            />
                                            <DragDropProvider />
                                            <CurrentTimeIndicator
                                                shadePreviousAppointments={true}
                                            />
                                        </Scheduler>
                                    </Paper>
                                </Box>

                            </CardContent>
                        </Card>
                    </Box>
                </div>
            </Box>
        </Fragment>
    );
}

export default Appointment;