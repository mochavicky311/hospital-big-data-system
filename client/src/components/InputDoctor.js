import React, {Fragment, useState } from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {registerDoctor, updateDoctor, updateReportData} from "../api/Api";


toast.configure();

const InputDoctor = ({doctor, classes}) => {
    const [medName, setMedName] = useState(doctor ? doctor.med_name : "");
    const [medEmail, setMedEmail] = useState(doctor ? doctor.med_email : "");
    const [medPassword, setMedPassword] = useState( "");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [medContact, setMedContact] = useState(doctor ? doctor.med_contact : "");
    const [emailErrorText, setEmailErrorText] = useState("");
    const [contactErrorText, setContactErrorText] = useState("");
    const [passwordErrorText, setPasswordErrorText] = useState("");
    const [changePassword, setChangePassword] = useState(false);
    const isValidForm = useState((emailErrorText === "" && contactErrorText === "" && passwordErrorText === ""));


    const handleEmailChange = e => {
        let email = e.target.value;
        let emailPattern = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);

        setMedEmail(email);

        if (emailPattern.test(email)) {
            setEmailErrorText("");
        }
        else {
            setEmailErrorText("Invalid Email Format");
        }
    }

    const handlePasswordMatch = e => {
        let ps = e.target.value;
        setConfirmPassword(ps);

        if (medPassword !== ps) {
            setPasswordErrorText("Password Not Match");
        } else {
            setPasswordErrorText("");
        }
    }

    const handleContactChange = e => {
        let contact = e.target.value;
        let contactPattern = new RegExp(/^(01)[0-9]{8,9}$/);

        setMedContact(contact);

        if (contactPattern.test(contact)) {
            setContactErrorText("");
        }
        else {
            setContactErrorText("Invalid Contact Number");
        }
    }

    const onSubmitForm = async e => {
        e.preventDefault();

        if(isValidForm){

            const body = {
                med_name: medName,
                med_email: medEmail,
                med_password: medPassword==="" ? "nochange" : medPassword,
                med_contact: medContact
            };

            if(!doctor){
                // register
                const parseRes = await registerDoctor(body);
                if(parseRes==="Created successfully!"){
                    toast.success(parseRes);
                    await updateReportData({
                        "current_doctors": "current_doctors + 1"
                    });
                    window.location.reload();
                } else {
                    toast.error(parseRes);
                }
            } else {
                // update doctor's details
                const parseRes = await updateDoctor(doctor.med_id, body);
                if(parseRes==="Updated successfully!"){
                    toast.success(parseRes);
                    window.location.reload();
                } else {
                    toast.error(parseRes);
                }
            }

        } else {
            toast.error("Input error.");
        }
    }

    return (
        <Fragment>
            <form>
                <TextField
                    fullWidth className={classes.inputField}
                    label="Name"
                    variant="outlined"
                    value={medName}
                    onChange={e => setMedName(e.target.value)}
                    required/>
                <TextField fullWidth className={classes.inputField}
                           type="email"
                           label="Email"
                           variant="outlined"
                           value={medEmail}
                           onChange={handleEmailChange}
                           required
                           error={emailErrorText !== ""}
                           helperText={emailErrorText}/>
                {
                    doctor ?
                        <FormControlLabel
                            control={
                                <Checkbox className={classes.inputField}
                                    checked={changePassword}
                                    onChange={e => setChangePassword(e.target.checked)}
                                    color="primary"
                                />
                            } label="Change password?" /> : ""
                }
                {
                    !doctor || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                                   type="password"
                                   label="Password"
                                   variant="outlined"
                                   value={medPassword}
                                   onChange={e => setMedPassword(e.target.value)}
                                   required /> : ""
                }
                {
                    !doctor || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                                   type="password"
                                   label="Confirm Password"
                                   variant="outlined"
                                   value={confirmPassword}
                                   error={passwordErrorText !== ""}
                                   helperText={passwordErrorText}
                                   onChange={e => handlePasswordMatch(e)}
                                   required /> : ""
                }
                <TextField fullWidth className={classes.inputField}
                           label="Contact"
                           variant="outlined"
                           value={medContact}
                           onChange={handleContactChange}
                           required
                           error={contactErrorText !== ""}
                           helperText={contactErrorText}/>
            </form>
            <Button onClick={onSubmitForm} color="primary">Save</Button>
        </Fragment>
    );
};

export default InputDoctor;