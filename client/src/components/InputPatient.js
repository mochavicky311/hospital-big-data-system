import React, {Fragment, useState, useEffect} from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import MenuItem from "@material-ui/core/MenuItem";
import InputAdornment from "@material-ui/core/InputAdornment";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {registerPatient, updatePatient, getAllDoctors, updateReportData} from "../api/Api";


toast.configure();

const InputPatient = ({patient, classes}) =>  {
    const [patientName, setPatientName] = useState(patient ? patient.patient_name : "");
    const [patientEmail, setPatientEmail] = useState(patient ? patient.patient_email : "");
    const [patientPassword, setPatientPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [patientContact, setPatientContact] = useState(patient ? patient.patient_contact : "");
    const [medId, setMedId] = useState(patient ? patient.med_id : "");
    const [patientAge, setPatientAge] = useState(patient ? new Date().getFullYear() - patient.patient_birth_year : "");
    const [patientGender, setPatientGender] = useState(patient ? patient.patient_gender : "");
    const [patientBirthYear, setPatientBirthYear] =useState(patient ? new Date(patient.patient_birth_year, 1, 1) : new Date());
    const [patientEmgcyNo, setPatientEmgcyNo] = useState(patient ? patient.patient_emgcy_no : "");
    const [patientHeightInM, setPatientHeightInM] = useState(patient ? patient.patient_height_m : "");
    const [patientWeightInKg, setPatientWeightInKg] = useState(patient ? patient.patient_weight_kg : "");
    const [disease, setDisease] = useState(patient ? patient.disease : "");
    const [isVegan, setIsVegan] = useState(patient ? patient.is_vegan : false);
    const [patientRace, setPatientRace] = useState(patient ? patient.patient_race : "");
    const [isMarried, setIsMarried] = useState(patient ? patient.is_married : false);
    const [docList, setDocList] = useState([]);
    const [passwordErrorText, setPasswordErrorText] = useState("");
    const [changePassword, setChangePassword] = useState(false);
    const [emailErrorText, setEmailErrorText] = useState("");
    const [contactErrorText, setContactErrorText] = useState("");
    const [emgcyErrorText, setEmgcyErrorText] = useState("");
    const [heightErrorText, setHeightErrorText] = useState("");
    const [weightErrorText, setWeightErrorText] = useState("");
    const isValidForm = useState(
        (emailErrorText === "" && contactErrorText === "" && emgcyErrorText === "" && heightErrorText === "" && weightErrorText === "" && passwordErrorText === ""))

    const handlePasswordMatch = e => {
        let ps = e.target.value;
        setConfirmPassword(ps);

        if (patientPassword !== ps) {
            setPasswordErrorText("Password Not Match");
        } else {
            setPasswordErrorText("");
        }
    }

    const handleBirthYearChanges = date => {
        if(date != null){
            let year = date.getFullYear();
            let currentYear = new Date().getFullYear();
            setPatientBirthYear(date);
            setPatientAge(currentYear - year);
        }
    }

    const handleIsVeganChange = e => {
        if(e.target.value)
            setIsVegan(true)
        else
            setIsVegan(false)
    }

    const handleIsMarriedChange = e => {
        if(e.target.value)
            setIsMarried(true)
        else
            setIsMarried(false)
    }

    const handlePatientEmailChange = e => {
        let email = e.target.value
        let emailPattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        setPatientEmail(email)

        if (emailPattern.test(email)) {
            setEmailErrorText("")
        }
        else {
            setEmailErrorText("Invalid Email Format")
        }
    }

    const handlePatientContactChange = e => {
        let contact = e.target.value
        let contactPattern = new RegExp(/^(01)[0-9]{8,9}$/);

        setPatientContact(contact)

        if (contactPattern.test(contact)) {
            setContactErrorText("")
        }
        else {
            setContactErrorText("Invalid Contact Number")
        }
    }

    const handlePatientEmgcyNoChange = e => {
        let contact = e.target.value
        let contactPattern = new RegExp(/^(01)[0-9]{8,9}$/);

        setPatientEmgcyNo(contact)

        if (contactPattern.test(contact)) {
            setEmgcyErrorText("")
        }
        else {
            setEmgcyErrorText("Invalid Contact Number")
        }
    }

    const handleHeightChange = e => {
        let height = e.target.value
        let rounded_height = Math.round(height * 100) / 100

        setPatientHeightInM(rounded_height.toString())

        if (parseFloat(height) >= 0 && parseFloat(height) <= 2.5) {
            setHeightErrorText("")
        }
        else {
            setHeightErrorText("Height must be between 0m to 2.5m.")
        }
    }

    const handleWeightChange = e => {
        let weight = e.target.value
        let rounded_weight = Math.round(weight * 10) / 10

        setPatientWeightInKg(rounded_weight.toString())

        if (parseFloat(weight) >= 0 && parseFloat(weight) <= 300) {
            setWeightErrorText("")
        }
        else {
            setWeightErrorText("Weight must be between 0 to 300kg.")
        }
    }

    const getDocList = async () => {
        setDocList([]);

        const allDoctors = await getAllDoctors();

        let newDocList = [];
        allDoctors.forEach((doctor) => {
            newDocList.push({value: doctor.med_id, label: doctor.med_name})
        });
        setDocList(newDocList);

    }


    const onSubmitForm = async e => {
        e.preventDefault();

        if(isValidForm){
            try {
                if(!patient){
                    const parseRes = await registerPatient({
                        patient_name: patientName,
                        patient_email: patientEmail,
                        patient_password: patientPassword==="" ? "nochange" : patientPassword,
                        patient_contact: patientContact,
                        med_id: medId,
                        patient_gender: patientGender,
                        patient_birth_year: patientBirthYear.getFullYear(),
                        patient_emgcy_no: patientEmgcyNo,
                        patient_height_m: patientHeightInM,
                        patient_weight_kg: patientWeightInKg,
                        patient_bmi: Math.round(patientWeightInKg/(patientHeightInM**2) * 10000) / 10000,
                        disease: disease,
                        is_vegan: isVegan,
                        patient_race: patientRace,
                        is_married: isMarried,
                        in_patient: true
                    })

                    const reportData = {
                        "current_patients": "current_patients + 1",
                        "total_admitted": "total_admitted + 1"
                    }

                    // update report data for gender
                    if(patientGender === 'M'){
                        reportData["male"] = "male + 1"
                    } else {
                        reportData["female"] = "female + 1"
                    }

                    // update report data for race
                    if(patientRace === 'M'){
                        reportData["malay"] = "malay + 1"
                    } else if(patientRace === 'C'){
                        reportData["chinese"] = "chinese + 1"
                    } else if(patientRace === 'I'){
                        reportData["indian"] = "indian + 1"
                    } else {
                        reportData["other"] = "other + 1"
                    }

                    // update report data for disease
                    if(disease === 1){
                        reportData["disease1"] = "disease1 + 1"
                    } else if(disease === 2){
                        reportData["disease2"] = "disease2 + 1"
                    } else if(disease === 3){
                        reportData["disease3"] = "disease3 + 1"
                    } else if(disease === 4) {
                        reportData["disease4"] = "disease4 + 1"
                    } else if(disease === 5){
                        reportData["disease5"] = "disease5 + 1"
                    } else {
                        reportData["disease6"] = "disease6 + 1"
                    }

                    // update report data for age group
                    if(patientAge < 18){
                        reportData["agegrp1"] = "agegrp1 + 1"
                    } else if(patientAge >= 18 && patientAge < 40 ){
                        reportData["agegrp2"] = "agegrp2 + 1"
                    } else if(patientAge >= 40 && patientAge < 65){
                        reportData["agegrp3"] = "agegrp3 + 1"
                    } else {
                        reportData["agegrp4"] = "agegrp4 + 1"
                    }

                    if(parseRes==="Created successfully!"){
                        toast.success(parseRes);
                        await updateReportData(reportData);
                        window.location.reload();
                    } else {
                        toast.error(parseRes);
                    }
                } else {
                    const parseRes = await updatePatient(patient.patient_id, {
                        patient_name: patientName,
                        patient_email: patientEmail,
                        patient_password: patientPassword==="" ? "nochange" : patientPassword,
                        patient_contact: patientContact,
                        med_id: medId,
                        patient_gender: patientGender,
                        patient_birth_year: patientBirthYear.getFullYear(),
                        patient_emgcy_no: patientEmgcyNo,
                        disease: disease,
                        is_vegan: isVegan,
                        patient_race: patientRace,
                        is_married: isMarried
                    });

                    if(parseRes==="Updated successfully!"){
                        toast.success(parseRes);
                        window.location.reload();
                    } else {
                        toast.error(parseRes);
                    }
                }
            }catch (err) {
                    console.error(err.message);
            }
        } else {
            toast.error("Input error.");
        }
    }


    useEffect(() => {
        getDocList();
    }, []);
    return (
        <Fragment>
            <form>
                <TextField
                    fullWidth className={classes.inputField}
                    label="Patient Name"
                    variant="outlined"
                    value={patientName}
                    onChange={e => setPatientName(e.target.value)}
                    required/>
                <TextField
                    className={classes.inputFieldHalf}
                    select
                    label="Doctor"
                    value={medId}
                    onChange={e => setMedId(e.target.value)}
                    variant="outlined"
                    required>
                    {docList.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
                <TextField fullWidth className={classes.inputField}
                           type="email"
                           label="Patient Email"
                           variant="outlined"
                           value={patientEmail}
                           onChange={handlePatientEmailChange}
                           required
                           error={emailErrorText !== ""}
                           helperText={emailErrorText}/>
                {
                    patient ?
                        <FormControlLabel
                            control={
                                <Checkbox className={classes.inputField}
                                          checked={changePassword}
                                          onChange={e => setChangePassword(e.target.checked)}
                                          color="primary"
                                />
                            } label="Change password?" /> : ""
                }
                {
                    !patient || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                                   type="password"
                                   label="Patient Password"
                                   variant="outlined"
                                   value={patientPassword}
                                   onChange={e => setPatientPassword(e.target.value)}
                                   required/> : ""
                }
                {
                    !patient || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                                   type="password"
                                   label="Confirm Password"
                                   variant="outlined"
                                   value={confirmPassword}
                                   error={passwordErrorText !== ""}
                                   helperText={passwordErrorText}
                                   onChange={e => handlePasswordMatch(e)}
                                   required/> : ""
                }
                <TextField fullWidth className={classes.inputField}
                           label="Patient Contact"
                           variant="outlined"
                           value={patientContact}
                           onChange={handlePatientContactChange}
                           required
                           error={contactErrorText !== ""}
                           helperText={contactErrorText}/>
                <TextField fullWidth className={classes.inputField}
                           label="Patient Emergency Contact"
                           variant="outlined"
                           value={patientEmgcyNo}
                           onChange={handlePatientEmgcyNoChange}
                           required
                           error={emgcyErrorText !== ""}
                           helperText={emgcyErrorText}/>
                <TextField
                    className={classes.inputFieldHalf}
                    select
                    label="Patient Gender"
                    value={patientGender}
                    onChange={e => setPatientGender(e.target.value)}
                    variant="outlined"
                    required>
                    {[{value: "M", label: "Male"}, {value: "F", label: "Female"}]
                        .map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
                <TextField
                    className={classes.inputFieldHalf}
                    select
                    label="Patient Race"
                    value={patientRace}
                    onChange={e => setPatientRace(e.target.value)}
                    variant="outlined"
                    required>
                    {[{value: "M", label: "Malay"}, {value: "C", label: "Chinese"}, {value: "I", label: "Indian"}, {value: "O", label: "Others"}]
                        .map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        className={classes.inputFieldHalf}
                        disableToolbar
                        variant="inline"
                        inputVariant="outlined"
                        format="yyyy"
                        views ={["year"]}
                        margin="normal"
                        label="Patient Birth Year"
                        value={patientBirthYear}
                        onChange={handleBirthYearChanges}
                        KeyboardButtonProps={{'aria-label': 'change date',}}
                        disableFuture
                        required
                    />
                </MuiPickersUtilsProvider>
                <TextField
                    disabled
                    className={classes.inputFieldHalf}
                    variant="filled"
                    label="Patient Age"
                    value={patientAge} />
                {patient ? "" :
                    <TextField
                        className={classes.inputFieldHalf}
                        type="number"
                        variant="outlined"
                        label="Patient Height"
                        value={patientHeightInM}
                        onChange={handleHeightChange}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">m</InputAdornment>,
                            inputProps: {min: 0, max: 2.5, step: 0.10}
                        }}
                        required
                        error={heightErrorText !== ""}
                        helperText={heightErrorText}/> }
                { patient ? "" :
                <TextField
                    className={classes.inputFieldHalf}
                    type="number"
                    variant="outlined"
                    label="Patient Weight"
                    value={patientWeightInKg}
                    InputProps={{
                        endAdornment: <InputAdornment position="end">kg</InputAdornment>,
                        inputProps: { min: 0, max: 300, step:0.5 }
                    }}
                    onChange={handleWeightChange}
                    required
                    error={weightErrorText !== ""}
                    helperText={weightErrorText}/> }
                <TextField
                    fullWidth
                    className={classes.inputField}
                    select
                    label="Disease"
                    value={disease}
                    onChange={e => setDisease(e.target.value)}
                    variant="outlined"
                    required >
                    {[{value: 1, label: "coronary heart disease"},
                        {value: 2, label: "arrhythmia"},
                        {value: 3, label: "cardiomyopathy"},
                        {value: 4, label: "heart failure"},
                        {value: 5, label: "heart infections and inflammation"},
                        {value: 6, label: "heart valve disease"}]
                        .map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
                <TextField
                    className={classes.inputFieldHalf}
                    select
                    label="Is this patient a vegan?"
                    value={isVegan}
                    onChange={handleIsVeganChange}
                    variant="outlined"
                    required >
                    {[{value: true, label: "Yes"},
                        {value: false, label: "No"},]
                        .map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
                <TextField
                    className={classes.inputFieldHalf}
                    select
                    label="Is this patient married?"
                    value={isMarried}
                    onChange={handleIsMarriedChange}
                    variant="outlined"
                    required >
                    {[{value: true, label: "Yes"},
                        {value: false, label: "No"},]
                        .map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                </TextField>
            </form>
            <Button onClick={onSubmitForm} color="primary">Save</Button>
        </Fragment>
    );
};

export default InputPatient;