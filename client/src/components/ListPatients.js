import React, {Fragment, useState, useEffect, useContext} from "react";
import Box from "@material-ui/core/Box";
import EditIcon from '@material-ui/icons/Edit';
import RemoveIcon from '@material-ui/icons/Remove';
import Card from "@material-ui/core/Card";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import Tooltip from '@material-ui/core/Tooltip';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import ViewCompactIcon from '@material-ui/icons/ViewCompact';
import AddIcon from '@material-ui/icons/Add';
import Chip from "@material-ui/core/Chip";
import { DataGrid } from '@material-ui/data-grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Fab from '@material-ui/core/Fab';
import theme from "../theme";
import Zoom from '@material-ui/core/Zoom';
import Grid from "@material-ui/core/Grid";
import clsx from "clsx";
import {
    dischargeOrAdmitPatient,
    getPatientsUnderDoctor,
    getAllPatients,
    getAllDiseases,
    updateReportData
} from "../api/Api";
import {UserContext} from "../UserContext";
import InputPatient from "./InputPatient";
import Dashboard from "./Dashboard";


toast.configure();

const ListPatients = ({ classes }) => {
    const currentUser = useContext(UserContext);
    const [patients, setPatients] = useState([]);
    const [diseases, setDiseases] = useState([]);
    const [openDialog, setOpenDialog] = React.useState(false);
    const [openEditDialog, setOpenEditDialog] = React.useState(false);
    const [editPatient, setEditPatient] = React.useState(null);
    const [selectedPatient, setSelectedPatient] = useState([]);
    const [listSelectedPatient, setListSelectedPatient] = useState([]);
    const [openDashboard, setOpenDashboard] = useState(false);
    const [rows, setRows] = useState([]);
    const [view, setView] = useState('card');
    const [rowSelected, setRowSelected] = useState(false);
    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    const columns = [
        { field: 'id', headerName: 'No', width: 100 },
        { field: 'name', headerName: 'Name', width: 300 },
        { field: 'gender', headerName: 'Gender', width: 100 },
        { field: 'age', headerName: 'Age', type: 'number', width: 100 },
        { field: 'disease', headerName: 'Disease', width: 300 },
        { field: 'status', headerName: 'Status', width: 150 }
    ];

    const setPatientsRows = () => {

        let newRows = [];

        patients.map((patient, i) => {
            return newRows.push({
                id: i+1,
                name: patient.patient_name,
                gender: patient.patient_gender,
                age: new Date().getFullYear() - patient.patient_birth_year,
                disease: diseases[patient.disease - 1],
                patient: patient,
                status: patient.in_patient ? "In hospital" : "Discharged"
            })
        })

        setRows(newRows);
    };

    const handleRowSelected = (patient) => {
        setListSelectedPatient(patient);
        setRowSelected(false);
        setTimeout(function(){setRowSelected(true)}, 100);
    };

    const handleOpenCreatePatientForm = () => {
        setOpenDialog(true);
    };

    const handleCloseCreatePatientForm = () => {
        setOpenDialog(false);
    };

    const handleOpenEditPatientForm = patient => {
        setEditPatient(patient);
        setOpenEditDialog(true);
    };

    const handleCloseEditPatientForm = () => {
        setOpenEditDialog(false);
    };

    const handleClickOpenDashboard = (patient) => {
        setSelectedPatient(patient);
        setOpenDashboard(true);
    }

    const handleCloseDashboard = () => {
        setOpenDashboard(false);
    }

    const getReportDataDict = (id, str) => {

        const reportData = {
            "current_patients": "current_patients" + str,
        }

        if(str === " + 1"){
            reportData["total_admitted"] = "total_admitted + 1";
        } else {
            reportData["total_discharges"] = "total_discharges + 1";
        }

        const p = patients.filter(patient => patient.id === id);

        // update report data for gender
        if(p.patient_gender === 'M'){
            reportData["male"] = "male" + str
        } else {
            reportData["female"] = "female" + str
        }

        // update report data for race
        if(p.patient_race === 'M'){
            reportData["malay"] = "malay" + str
        } else if(p.patient_race === 'C'){
            reportData["chinese"] = "chinese" + str
        } else if(p.patient_race === 'I'){
            reportData["indian"] = "indian" + str
        } else {
            reportData["other"] = "other" + str
        }

        // update report data for disease
        if(p.disease === 1){
            reportData["disease1"] = "disease1" + str
        } else if(p.disease === 2){
            reportData["disease2"] = "disease2" + str
        } else if(p.disease === 3){
            reportData["disease3"] = "disease3" + str
        } else if(p.disease === 4) {
            reportData["disease4"] = "disease4" + str
        } else if(p.disease === 5){
            reportData["disease5"] = "disease5" + str
        } else {
            reportData["disease6"] = "disease6" + str
        }

        // update report data for age group
        const age = new Date().getFullYear() - p.patient_birth_year;
        if(age < 18){
            reportData["agegrp1"] = "agegrp1" + str
        } else if(age >= 18 && age < 40 ){
            reportData["agegrp2"] = "agegrp2" + str
        } else if(age >= 40 && age < 65){
            reportData["agegrp3"] = "agegrp3" + str
        } else {
            reportData["agegrp4"] = "agegrp4" + str
        }

        return reportData;

    }

    const dischargePatient = async (id, value) => {
        // value: true -> readmit, false -> discharge
        if (window.confirm('Confirm?')) {

            let body;
            if(value) {
                body = getReportDataDict(id, " + 1");
            } else {
                body = getReportDataDict(id, " - 1");
            }

            const parseRes = await dischargeOrAdmitPatient(id, value);

            if (parseRes === "Success!") {
                const updated = await updateReportData(body);

                if(updated === "Updated successfully!"){
                    toast.success(updated);
                } else {
                    toast.error(updated);
                }

            } else {
                toast.error(parseRes);
            }
        }
    }


    const handleViewChange = (event, nextView) => {
        setView(nextView);
    };


    useEffect(() => {

        const getPatients = async () => {

            try {
                let jsonData;
                if (currentUser.user_role === "A" || currentUser.user_role === "N") {
                    jsonData = await getAllPatients();
                } else if (currentUser.user_role === "D") {
                    jsonData = await getPatientsUnderDoctor(currentUser.user_id);
                }

                if (!jsonData) {
                    toast.error("Error occurred");
                }

                return jsonData;

            } catch (err) {
                console.error(err.message);
            }
        }

        const getDiseases = async () => {

            const jsonData = await getAllDiseases();
            const diseasesArray = jsonData.map(({disease}) => disease);

            if (!jsonData) {
                toast.error("Error occurred");
            }

            return diseasesArray;
        }

        getPatients().then(res => setPatients(res));
        getDiseases().then(res => setDiseases(res));

    }, [currentUser.user_id, currentUser.user_role]);


    return (
        <Fragment>
            <Box marginY={3}>
                <Typography variant="h3" gutterBottom>Patients</Typography>
            </Box>

            <Box width="100%">
                <Grid container justify="space-between">
                <Grid item>
                    {currentUser.user_role === "A" ?
                        (<Tooltip title="Admit new patient">
                            <Button className={classes.btnItem}
                                    onClick={handleOpenCreatePatientForm}
                                    variant="contained" size="small" color="primary"
                                    startIcon={<AddIcon/>}>
                                new patient
                            </Button>
                        </Tooltip>) : ""
                    }
                </Grid>
                <Grid item>
                    <ToggleButtonGroup color="primary" value={view} exclusive onChange={handleViewChange}>
                        <ToggleButton value="card"><ViewModuleIcon/></ToggleButton>
                        <ToggleButton value="list" onClick={setPatientsRows}><ListAltIcon/></ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
            </Grid>
            </Box>

            <Box marginY={3} style={{ height: "auto", width: '100%' }} className={view==="card" ? classes.hide : ""}>

                <Zoom in={rowSelected} timeout={transitionDuration}>
                <Box className={clsx(classes.fabContainer, {
                    [classes.hide]: !rowSelected,
                })}>
                    {currentUser.user_role === "A" || currentUser.user_role === "D" ?
                        (<Tooltip title="Edit patient">
                            <Fab className={classes.fab}
                                 color="primary" aria-label="edit"
                                onClick={() => handleOpenEditPatientForm(listSelectedPatient)}
                            >
                                <EditIcon/>
                            </Fab>
                        </Tooltip>) : ""}
                    {currentUser.user_role === "A" ?
                        listSelectedPatient.in_patient ?
                            (<Tooltip title="Discharge patient">
                                <Fab className={classes.fab}
                                     color="secondary" aria-label="discharge"
                                     onClick={() => {
                                         dischargePatient(listSelectedPatient.patient_id, false);
                                         window.location.reload();
                                     }}
                                >
                                    <RemoveIcon/>
                                </Fab>
                            </Tooltip>) :
                            (<Tooltip title="Readmit patient">
                                <Fab className={classes.fab}
                                     color="secondary" aria-label="readmit"
                                     onClick={() => {
                                         dischargePatient(listSelectedPatient.patient_id, true);
                                         window.location.reload();
                                     }}
                                >
                                    <AddIcon/>
                                </Fab>
                            </Tooltip>)
                        : ""
                    }
                    { currentUser.user_role === 'A' ? "" :
                        <Tooltip title="View dashboard">
                            <Fab className={classes.fab}
                                 variant="extended" aria-label="view"
                                onClick={() => handleClickOpenDashboard(listSelectedPatient)}
                            >
                                <Box marginRight={1} marginTop={1}><ViewCompactIcon/></Box>
                                Dashboard
                            </Fab>
                        </Tooltip>}
                </Box>
                </Zoom>

                <DataGrid hideFooterSelectedRowCount
                          autoHeight
                          rows={rows} columns={columns} pageSize={10}
                          onRowSelected={e => {
                              handleRowSelected(e.data.patient);
                          }}
                />
            </Box>

            <Box display="flex" flexWrap="wrap" className={view==="list" ? classes.hide : ""}>
                {patients.map(patient => (
                    <Card key={`${patient.patient_id}`} className={classes.card} variant="outlined" align="center">
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                {patient.patient_name}
                            </Typography>
                            <Typography variant="subtitle2">
                                {patient.patient_gender === "M" ? "Male" : "Female"}, {new Date().getFullYear() - patient.patient_birth_year} years old
                            </Typography>
                            <Chip className={classes.chip} color="secondary" variant="outlined" size="small"
                                  label={diseases[patient.disease - 1]}/>
                        </CardContent>
                        <CardActions className={classes.cardBtn}>
                            {currentUser.user_role === "A" || currentUser.user_role === "D" ?
                                (<Tooltip title="Edit patient">
                                    <IconButton className={classes.cardBtnItem}
                                                onClick={() => handleOpenEditPatientForm(patient)}
                                                color="primary" aria-label="edit">
                                        <EditIcon/>
                                    </IconButton>
                                </Tooltip>) : ""}

                            {currentUser.user_role === "A" ?
                                patient.in_patient ?
                                    (<Tooltip title="Discharge patient">
                                        <Fab className={classes.fab}
                                             color="secondary" aria-label="discharge" size="small"
                                             onClick={() => {
                                                 dischargePatient(patient.patient_id, false);
                                                 window.location.reload();
                                             }}
                                        >
                                            <RemoveIcon/>
                                        </Fab>
                                    </Tooltip>) :
                                    (<Tooltip title="Readmit patient">
                                        <Fab className={classes.fab}
                                             color="secondary" aria-label="readmit" size="small"
                                             onClick={() => {
                                                 dischargePatient(patient.patient_id, true);
                                                 window.location.reload();
                                             }}
                                        >
                                            <AddIcon/>
                                        </Fab>
                                    </Tooltip>)
                                : ""
                            }
                            { currentUser.user_role === 'A' ? "" :
                            <Tooltip title="View dashboard">
                                <Button className={classes.cardBtnItem} variant="contained"
                                        onClick={() => handleClickOpenDashboard(patient)}
                                        size="large" aria-label="view" startIcon={<ViewCompactIcon/>}>
                                    Dashboard
                                </Button>
                            </Tooltip>}
                        </CardActions>
                    </Card>
                ))}
            </Box>

            <Dialog open={openDialog} onClose={handleCloseCreatePatientForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Create New Patient</DialogTitle>
                </Typography>
                <DialogContent>
                    <InputPatient patient={null} classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseCreatePatientForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>


            <Dialog open={openEditDialog} onClose={handleCloseEditPatientForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Edit Patient Details</DialogTitle>
                </Typography>

                <DialogContent>
                    <InputPatient patient={editPatient} classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEditPatientForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

            <Dialog fullScreen open={openDashboard} onClose={handleCloseDashboard}>
                <DialogContent className={classes.dashboard}>
                    <Button variant="outlined" onClick={handleCloseDashboard} color="primary">Close</Button>
                    <Dashboard patient={selectedPatient} diseases={diseases} classes={classes} />
                </DialogContent>
            </Dialog>

        </Fragment>
    );
}

export default ListPatients;