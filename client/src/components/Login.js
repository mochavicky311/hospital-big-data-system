import React, {Fragment, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';


toast.configure();

const Login = ({setAuth, classes }) => {

    const [inputs, setInputs] = useState({
        "email": "",
        "password": "",
        "user_role": ""
    })

    const { email, password, user_role } = inputs

    const onChange = e => {
        setInputs({ ...inputs, [e.target.name]: e.target.value })
    }

    const onSubmitForm = async e => {
        e.preventDefault();
        try {
            const body = { email, password };
            let parseRes;

            if(user_role === "M"){
                const response = await fetch("http://localhost:5000/auth/loginDoctor", {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(body)
                });
                parseRes = await response.json();
            } else if(user_role === "P"){
                const response = await fetch("http://localhost:5000/auth/loginPatient", {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(body)
                });
                parseRes = await response.json();
            } else if(user_role === "A"){
                const response = await fetch("http://localhost:5000/auth/loginAdmin", {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(body)
                });
                parseRes = await response.json();
            } else {
                const response = await fetch("http://localhost:5000/auth/loginNurse", {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(body)
                });
                parseRes = await response.json();
            }

            if(parseRes.token && parseRes.id){
                localStorage.setItem("token", parseRes.token);
                localStorage.setItem("id", parseRes.id)

                setAuth(true);

                toast.success("Login successfully!");
            } else {
                setAuth(false);
                toast.error(parseRes);
            }
        } catch (err) {
            console.error(err.message);
            toast.error("Error occurred.");
        }
    }

    return (
        <Fragment>
            <Paper className={classes.loginBackground} elevation={0}>
                <Box width="100%">
                    <Grid container direction="row" justify="center" alignItems="center" spacing={2} style={{ minHeight: '100vh', maxWidth: '100vw' }}>
                        <Grid item md={6}>
                            <Box marginY={5} padding={2} className={classes.mainTitle}>
                                <Typography variant="h1" align="center">Habit-Change Hospital Big Data Analytics System</Typography>
                            </Box>
                        </Grid>
                        <Grid item md={4}>
                            <Paper elevation={5}>
                                <Box paddingX={5} paddingY={10}>
                                    <TextField fullWidth className={classes.inputField}
                                               size="small"
                                               type="email"
                                               name="email"
                                               label="Email"
                                               variant="outlined"
                                               value={inputs.email}
                                               onChange={e => onChange(e)}
                                               required />
                                    <TextField fullWidth className={classes.inputField}
                                               size="small"
                                               type="password"
                                               name="password"
                                               label="Password"
                                               variant="outlined"
                                               value={inputs.password}
                                               onChange={e => onChange(e)}
                                               required/>
                                    <TextField
                                        fullWidth className={classes.inputField}
                                        size="small"
                                        select
                                        label="User Role"
                                        name="user_role"
                                        value={inputs.user_role}
                                        onChange={e => onChange(e)}
                                        variant="outlined"
                                        required>
                                        {[{value: "M", label: "Doctor"}, {value: "D", label: "Nurse / Medical Staffs"}, {value: "P", label: "Patient"}, {value: "A", label: "Admin"}]
                                            .map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                    </TextField>
                                    <Button fullWidth className={classes.addMargin} onClick={onSubmitForm} color="primary" variant="contained">
                                        Login
                                    </Button>
                                </Box>
                            </Paper>
                        </Grid>
                    </Grid>
                </Box>
            </Paper>

        </Fragment>
    )
}

export default Login;