import React, {Fragment, useEffect, useState} from "react";
import {toast} from "react-toastify";
import Box from "@material-ui/core/Box";
import {ResponsiveLine} from "@nivo/line";
import Link from "@material-ui/core/Link";

toast.configure();

const EcgSignalGraph = ({filename, filepath}) => {

    const [signal, setSignal] = useState([]); // storing the uploaded file

    useEffect(() => {
        const getEcgData = async () => {
            try {
                const response = await fetch(`http://localhost:5000/ecg/${filename}`, {
                    method: "GET",
                    headers: { token: localStorage.token }
                });
                const jsonData = await response.json();

                if(!jsonData){
                    toast.error("Error occurred");
                } else {
                    let data = [];
                    let x = 0;
                    jsonData.ECG.forEach( row => {
                        data.push({x: x, y: parseFloat(row)});
                        x += 60 / 930;
                    });
                    setSignal(data);
                }
            } catch (err) {
                console.error(err.message);
            }
        }
        getEcgData();
    }, [filename]);


    return (
        <Fragment>
            <Link href={filepath}>Download ECG Data</Link>


            <Box margin={3} height="300px">
                <ResponsiveLine
                    data={[
                        {
                            "id": "ECG",
                            "data": signal
                        }
                    ]}
                    margin={{ top: 20, right: 10, bottom: 60, left: 60 }}
                    xScale={{ type: 'linear'}}
                    yScale={{ type: 'linear', min: '-3.0', max: '3.0', stacked: false, reverse: false }}
                    yFormat=" >-.2f"
                    axisBottom={{
                        tickValues: [0, 15, 30, 45, 60],
                        orient: 'bottom',
                        tickSize: 5,
                        tickPadding: 5,
                        legend: 'Time (seconds)',
                        legendOffset: 40,
                        legendPosition: 'middle'
                    }}
                    axisLeft={{
                        orient: 'left',
                        tickPadding: 5,
                        legend: 'Voltage (mV)',
                        legendOffset: -40,
                        legendPosition: 'middle'
                    }}
                    enablePoints={false}
                    colors={{ "scheme": "pastel1" }}
                    useMesh={true}
                    isInteractive={false}
                    animate={false}
                /></Box>
        </Fragment>
    );

}

export default EcgSignalGraph;