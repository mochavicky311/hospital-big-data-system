import React, { Fragment, useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Card from "@material-ui/core/Card";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import Tooltip from '@material-ui/core/Tooltip';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import theme from "../theme";
import Grid from "@material-ui/core/Grid";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import ListAltIcon from "@material-ui/icons/ListAlt";
import Zoom from "@material-ui/core/Zoom";
import clsx from "clsx";
import Fab from "@material-ui/core/Fab";
import {DataGrid} from "@material-ui/data-grid";
import {deleteADoctor, getAllDoctors, updateReportData} from "../api/Api";


// components
import InputDoctor from "./InputDoctor";

toast.configure();

const ListDoctors = ({classes}) => {
    const [doctors, setDoctors] = useState([]);
    const [openDialog, setOpenDialog] = React.useState(false);
    const [openEditDialog, setOpenEditDialog] = React.useState(false);
    const [editDoctor, setEditDoctor] = React.useState(null);
    const [rows, setRows] = useState([]);
    const [view, setView] = useState('card');
    const [listSelectedDoctor, setListSelectedDoctor] = useState([]);
    const [rowSelected, setRowSelected] = useState(false);
    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    const columns = [
        { field: 'id', headerName: 'No', width: 100 },
        { field: 'name', headerName: 'Name', width: 300 },
        { field: 'email', headerName: 'Email', width: 300},
        { field: 'contact', headerName: 'Contact Number', width: 300}
    ];

    const setDoctorsRows = () => {

        let newRows = [];

        doctors.map((doctor, i) => {
            return newRows.push({
                id: i+1,
                name: doctor.med_name,
                email: doctor.med_email,
                contact: doctor.med_contact,
                doctor: doctor
            })
        })

        setRows(newRows);
    };

    const handleRowSelected = doctor => {
        setListSelectedDoctor(doctor);
        setRowSelected(false);
        setTimeout(function(){setRowSelected(true)}, 100);
    };

    const handleOpenCreateDoctorForm = () => {
        setOpenDialog(true);
    };

    const handleCloseCreateDoctorForm = () => {
        setOpenDialog(false);
    };

    const handleOpenEditDoctorForm = doctor => {
        setEditDoctor(doctor);
        setOpenEditDialog(true);
    };

    const handleCloseEditDoctorForm = () => {
        setOpenEditDialog(false);
    };

    const handleViewChange = (event, nextView) => {
        setView(nextView);
    };

    const deleteDoctor = async (id) => {
        if (window.confirm('Confirm delete?')) {
            const parseRes = await deleteADoctor(id);

            if(parseRes === "Deleted successfully!"){
                await updateReportData({
                    "current_doctors": "current_doctors - 1"
                });
                setDoctors(doctors.filter(doctor => doctor.med_id !== id))
                toast.success(parseRes);
            } else {
                toast.error(parseRes);
            }
        }
    }

    useEffect(() => {

        const getDoctors = async () => {

            const jsonData = await getAllDoctors();
            setDoctors(jsonData);

            if(!jsonData){
                toast.error("Error occurred");
            }

        }

        getDoctors();
    }, []);

    return (
        <Fragment>
            <Box marginY={3}>
                <Typography variant="h3" gutterBottom>Doctors</Typography>
            </Box>

            <Box width="100%">
                <Grid container justify="space-between">
                <Grid item>
                    <Tooltip title="Create new doctor">
                        <Button className={classes.btnItem}
                                onClick={handleOpenCreateDoctorForm}
                                variant="contained" size="small" color="primary"
                                startIcon={<AddIcon/>}>
                            new doctor
                        </Button>
                    </Tooltip>
                </Grid>
                <Grid item>
                    <ToggleButtonGroup color="primary" value={view} exclusive onChange={handleViewChange}>
                        <ToggleButton value="card"><ViewModuleIcon/></ToggleButton>
                        <ToggleButton value="list" onClick={setDoctorsRows}><ListAltIcon/></ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
            </Grid>
            </Box>

            <Box marginY={3} style={{ height: "auto", width: '100%' }} className={view==="card" ? classes.hide : ""}>

                <Zoom in={rowSelected} timeout={transitionDuration}>
                    <Box className={clsx(classes.fabContainer, {
                        [classes.hide]: !rowSelected,
                    })}>
                        <Tooltip title="Edit doctor">
                            <Fab className={classes.fab}
                                 color="primary" aria-label="edit"
                                 onClick={() => handleOpenEditDoctorForm(listSelectedDoctor)}
                            >
                                <EditIcon />
                            </Fab>
                        </Tooltip>
                        <Tooltip title="Delete doctor">
                            <Fab className={classes.fab}
                                 color="secondary" aria-label="delete"
                                 onClick={() => {
                                     deleteDoctor(listSelectedDoctor.med_id);
                                     window.location.reload();
                                 }}>
                                <DeleteForeverIcon />
                            </Fab>
                        </Tooltip>
                    </Box>
                </Zoom>

                <DataGrid hideFooterSelectedRowCount
                          autoHeight
                          rows={rows} columns={columns} pageSize={10}
                          onRowSelected={e => {
                              handleRowSelected(e.data.doctor);
                          }}
                />
            </Box>

            <Box display="flex" flexWrap="wrap" className={view==="list" ? classes.hide : ""}>
                { doctors.map(doctor => (
                    <Card key={`${doctor.med_id}`} className={classes.card} variant="outlined" align="center">
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                {doctor.med_name}
                            </Typography>
                            <Typography variant="subtitle2">
                                Contact No: {doctor.med_contact }
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardBtn}>
                            <Tooltip title="Edit doctor">
                                <IconButton className={classes.cardBtnItem}
                                            onClick={() => handleOpenEditDoctorForm(doctor)}
                                            size="small" color="primary" aria-label="edit">
                                    <EditIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Delete doctor">
                                <IconButton className={classes.cardBtnItem}
                                            onClick={() => deleteDoctor(doctor.med_id)}
                                            size="small" color="secondary" aria-label="delete">
                                    <DeleteForeverIcon />
                                </IconButton>
                            </Tooltip>
                        </CardActions>
                    </Card>
                ))}
            </Box>

            <Dialog open={openDialog} onClose={handleCloseCreateDoctorForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Create New Doctor</DialogTitle>
                </Typography>
                <DialogContent>
                    <InputDoctor doctor={ null } classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseCreateDoctorForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>


            <Dialog open={openEditDialog} onClose={handleCloseEditDoctorForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Edit Doctor Details</DialogTitle>
                </Typography>
                <DialogContent>
                    <InputDoctor doctor={ editDoctor } classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEditDoctorForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

        </Fragment>
    );
}

export default ListDoctors;
