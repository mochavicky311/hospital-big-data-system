import React, { Fragment, useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Card from "@material-ui/core/Card";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import Tooltip from '@material-ui/core/Tooltip';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import ListAltIcon from "@material-ui/icons/ListAlt";
import Grid from "@material-ui/core/Grid";
import theme from "../theme";
import Zoom from "@material-ui/core/Zoom";
import clsx from "clsx";
import {DataGrid} from "@material-ui/data-grid";
import Fab from "@material-ui/core/Fab";

// components
import InputNurse from "./InputNurse";
import {deleteANurse, getAllNurses} from "../api/Api";


toast.configure();

const ListNurse = ({classes}) => {
    const [ nurse, setNurse ] = useState([]);
    const [openDialog, setOpenDialog] = React.useState(false);
    const [openEditDialog, setOpenEditDialog] = React.useState(false);
    const [editNurse, setEditNurse] = React.useState(null);
    const [rows, setRows] = useState([]);
    const [view, setView] = useState('card');
    const [listSelectedNurse, setListSelectedNurse] = useState([]);
    const [rowSelected, setRowSelected] = useState(false);
    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    const columns = [
        { field: 'id', headerName: 'No', width: 100 },
        { field: 'name', headerName: 'Name', width: 300 },
        { field: 'email', headerName: 'Email', width: 300}
    ];

    const setNursesRows = () => {

        let newRows = [];

        nurse.map((n, i) => {
            return newRows.push({
                id: i+1,
                name: n.nurse_name,
                email: n.nurse_email,
                nurse: n
            })
        })

        setRows(newRows);
    };

    const handleRowSelected = n => {
        setListSelectedNurse(n);
        setRowSelected(false);
        setTimeout(function(){setRowSelected(true)}, 100);
    };

    const handleOpenCreateForm = () => {
        setOpenDialog(true);
    };

    const handleCloseCreateForm = () => {
        setOpenDialog(false);
    };

    const handleOpenEditForm = nurse => {
        setEditNurse(nurse);
        setOpenEditDialog(true);
    };

    const handleCloseEditForm = () => {
        setOpenEditDialog(false);
    };

    const handleViewChange = (event, nextView) => {
        setView(nextView);
    };

    const deleteNurse = async (id) => {
        if (window.confirm('Confirm delete?')) {

            const parseRes = await deleteANurse(id);

            if(parseRes === "Deleted successfully!"){
                setNurse(nurse.filter(n => n.nurse_id !== id))
                toast.success(parseRes);
            } else {
                toast.error(parseRes);
            }

        }
    }

    const getNurses = async () => {

        const jsonData = await getAllNurses();
        setNurse(jsonData);

        if(!jsonData){
            toast.error("Error occurred");
        }
    }

    useEffect(() => {
        getNurses();
    }, []);

    return (
        <Fragment>
            <Box marginY={3}>
                <Typography variant="h3" gutterBottom>Nurses and Medical Staffs</Typography>
            </Box>

            <Box width="100%">
                <Grid container justify="space-between">
                <Grid item>
                    <Tooltip title="Create new nurse / medical staff">
                        <Button className={classes.btnItem}
                                onClick={handleOpenCreateForm}
                                variant="contained" size="small" color="primary"
                                startIcon={<AddIcon/>}>
                            new nurse / medical staff
                        </Button>
                    </Tooltip>
                </Grid>
                <Grid item>
                    <ToggleButtonGroup color="primary" value={view} exclusive onChange={handleViewChange}>
                        <ToggleButton value="card"><ViewModuleIcon/></ToggleButton>
                        <ToggleButton value="list" onClick={setNursesRows}><ListAltIcon/></ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
            </Grid>
            </Box>

            <Box marginY={3} style={{ height: "auto", width: '100%' }} className={view==="card" ? classes.hide : ""}>

                <Zoom in={rowSelected} timeout={transitionDuration}>
                    <Box className={clsx(classes.fabContainer, {
                        [classes.hide]: !rowSelected,
                    })}>
                        <Tooltip title="Edit nurse / medical staff">
                            <Fab className={classes.fab}
                                 color="primary" aria-label="edit"
                                 onClick={() => handleOpenEditForm(listSelectedNurse)}
                            >
                                <EditIcon />
                            </Fab>
                        </Tooltip>
                        <Tooltip title="Delete nurse / medicall staff">
                            <Fab className={classes.fab}
                                 color="secondary" aria-label="delete"
                                 onClick={() => {
                                     deleteNurse(listSelectedNurse.nurse_id)
                                     window.location.reload();
                                 }}
                            >
                                <DeleteForeverIcon />
                            </Fab>
                        </Tooltip>
                    </Box>
                </Zoom>

                <DataGrid hideFooterSelectedRowCount
                          autoHeight
                          rows={rows} columns={columns} pageSize={10}
                          onRowSelected={e => {
                              handleRowSelected(e.data.nurse);
                          }}
                />
            </Box>

            <Box display="flex" flexWrap="wrap" className={view==="list" ? classes.hide : ""}>
                { nurse.map(n => (
                    <Card key={`${n.nurse_id}`} className={classes.card} variant="outlined" align="center">
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                {n.nurse_name}
                            </Typography>
                            <Typography variant="subtitle2">
                                {n.nurse_email}
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardBtn}>
                            <Tooltip title="Edit nurse / medical staff">
                                <IconButton className={classes.cardBtnItem}
                                            onClick={() => handleOpenEditForm(n)}
                                            size="small" color="primary" aria-label="edit">
                                    <EditIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Delete nurse / medicall staff">
                                <IconButton className={classes.cardBtnItem}
                                            onClick={() => deleteNurse(n.nurse_id)}
                                            size="small" color="secondary" aria-label="delete">
                                    <DeleteForeverIcon />
                                </IconButton>
                            </Tooltip>
                        </CardActions>
                    </Card>
                ))}
            </Box>

            <Dialog open={openDialog} onClose={handleCloseCreateForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Create New Nurse / Medical Staff</DialogTitle>
                </Typography>
                <DialogContent>
                    <InputNurse nurse={ null } classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseCreateForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>


            <Dialog open={openEditDialog} onClose={handleCloseEditForm} className={classes.dialog}>
                <Typography variant="h4" gutterBottom>
                    <DialogTitle disableTypography>Edit Nurse / Medical Staff Details</DialogTitle>
                </Typography>

                <DialogContent>
                    <InputNurse nurse={ editNurse } classes={classes}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEditForm} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>

        </Fragment>
    );
}

export default ListNurse;
