import React, {Fragment, useState } from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {registerNurse, updateNurse} from "../api/Api";


toast.configure();

const InputNurse = ({nurse, classes}) => {
    const [name, setName] = useState(nurse ? nurse.nurse_name : "");
    const [email, setEmail] = useState(nurse ? nurse.nurse_email : "");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [emailErrorText, setEmailErrorText] = useState("");
    const [passwordErrorText, setPasswordErrorText] = useState("");
    const [changePassword, setChangePassword] = useState(false);


    const handleEmailChange = e => {
        let email = e.target.value;
        let emailPattern = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);

        setEmail(email);

        if (emailPattern.test(email)) {
            setEmailErrorText("");
        } else {
            setEmailErrorText("Invalid Email Format");
        }
    }

    const handlePasswordMatch = e => {
        let ps = e.target.value;
        setConfirmPassword(ps);

        if (password !== ps) {
            setPasswordErrorText("Password Not Match");
        } else {
            setPasswordErrorText("");
        }
    }

    const onSubmitForm = async e => {
        e.preventDefault();

        if (emailErrorText === "" && passwordErrorText === "") {
            try {
                const body = {
                    nurse_name: name,
                    nurse_email: email,
                    nurse_password: password === "" ? "nochange" : password
                };

                if (!nurse) {
                    const parseRes = await registerNurse(body);

                    if (parseRes === "Created successfully!") {
                        toast.success(parseRes);
                        window.location.reload();
                    } else {
                        toast.error(parseRes);
                    }
                } else {
                    const parseRes = await updateNurse(nurse.nurse_id, body)
                    if (parseRes === "Updated successfully!") {
                        toast.success(parseRes);
                        window.location.reload();
                    } else {
                        toast.error(parseRes);
                    }
                }
            } catch (err) {
                console.error(err.message);
            }
        } else {
            toast.error("Input error.");
        }
    }

    return (
        <Fragment>
            <form>
                <TextField
                    fullWidth className={classes.inputField}
                    label="Name"
                    variant="outlined"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required/>
                <TextField fullWidth className={classes.inputField}
                           type="email"
                           label="Email"
                           variant="outlined"
                           value={email}
                           onChange={handleEmailChange}
                           required
                           error={emailErrorText !== ""}
                           helperText={emailErrorText}/>
                {
                    nurse ?
                        <FormControlLabel
                            control={
                            <Checkbox className={classes.inputField}
                                checked={changePassword}
                                onChange={e => setChangePassword(e.target.checked)}
                                color="primary"
                            />
                            } label="Change password?" /> : ""
                }
                {
                    !nurse || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                                   type="password"
                                   label="Password"
                                   variant="outlined"
                                   value={password}
                                   onChange={e => setPassword(e.target.value)}
                                   required/> : ""
                }
                {
                    !nurse || changePassword ?
                        <TextField fullWidth className={classes.inputField}
                           type="password"
                           label="Confirm Password"
                           variant="outlined"
                           value={confirmPassword}
                           error={passwordErrorText !== ""}
                           helperText={passwordErrorText}
                           onChange={e => handlePasswordMatch(e)}
                           required/> : ""
                }

            </form>
            <Button onClick={onSubmitForm} color="primary">Save</Button>
        </Fragment>
    );
}

export default InputNurse;
