import React, {Fragment, useContext, useEffect, useState} from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import HomeIcon from '@material-ui/icons/Home';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {UserContext} from "../UserContext";
import Dashboard from "./Dashboard";
import Appointment from "./Appointment";
import {getAllDiseases, getAPatient} from "../api/Api";
import Box from "@material-ui/core/Box";


toast.configure();

const PatientSideMenu = ({setAuth, classes}) => {
    const currentUser = useContext(UserContext);
    const [ open, setOpen ] = useState(false);
    const [ index, setIndex ] = useState(0);
    const [ patient, setPatient ] = useState([]);
    const [ loading, setLoading ] = useState(true);
    const [ diseases, setDiseases ] = useState([]);


    const logout = async e => {
        e.preventDefault();
        try {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            setAuth(false);
            window.location.replace("/");
            toast.success("Logout successfully!");
        } catch (err){
            console.error(err.message);
        }
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleClickMenuItem = index => {
        setIndex(index);
    };

    const setDefaultIndex = () => {
        if(window.location.pathname === "/appointments"){
            setIndex(1);
        } else if(window.location.pathname === "/notifications"){
            setIndex(2);
        } else {
            setIndex(0);
        }
    }

    useEffect(() => {

        const getPatient = async () => {
            const jsonData = await getAPatient(currentUser.user_id);

            if(!jsonData){
                toast.error("Error occurred");
            }

            return jsonData;
        };

        const getDiseases = async () => {

            const jsonData = await getAllDiseases();

            if(!jsonData){
                toast.error("Error occurred");
            }

            return jsonData.map(({disease}) => disease);
        };

        setDefaultIndex();

        getPatient().then(res => {
            setPatient(res);
            setLoading(false);
        });
        getDiseases().then(res => setDiseases(res));
    }, [currentUser.user_id]);

    return (
       <Fragment>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit" aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}>
                        <MenuIcon />
                    </IconButton>

                    <Box width="100%">
                        <Grid container justify="space-between">
                            <Grid item>
                                <Typography variant="h4" noWrap>Hello, {patient.patient_name}!</Typography>
                            </Grid>
                            <Grid item>
                                <Button onClick={e => logout(e)} color="inherit">Logout</Button>
                            </Grid>
                        </Grid>
                    </Box>

                </Toolbar>
            </AppBar>

            <Router>
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: open,
                            [classes.drawerClose]: !open,
                        }),
                    }}>
                    <div className={classes.toolbar}>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <MenuList>
                        <Link className={classes.link} to="/">
                            <MenuItem button selected={index === 0} onClick={() => handleClickMenuItem(0)}>
                                <ListItemIcon>
                                    <HomeIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography={true} primary="Dashboard" />
                                </Typography>

                            </MenuItem>
                        </Link>
                        <Link className={classes.link} to="/appointments">
                            <MenuItem button selected={index === 1} onClick={() => handleClickMenuItem(1)}>
                                <ListItemIcon>
                                    <EventAvailableIcon />
                                </ListItemIcon>
                                <Typography variant="button">
                                    <ListItemText disableTypography={true} primary="Appointment" />
                                </Typography>

                            </MenuItem>
                        </Link>
                    </MenuList>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Switch>
                        <Route exact path="/">
                            <Fragment>
                                {loading ? "" : <Dashboard patient={patient} diseases={diseases} classes={classes} />}
                            </Fragment>
                        </Route>
                        <Route exact path="/appointments">
                            <Fragment>
                                {loading ? "" : <Appointment user_info={patient} classes={classes} />}
                            </Fragment>
                        </Route>
                    </Switch>
                </main>
            </Router>
       </Fragment>
    );
}

export default PatientSideMenu;
