import React, {Fragment, useContext, useEffect, useState} from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import HomeIcon from '@material-ui/icons/Home';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {UserContext} from "../UserContext";
import {getMedName} from "../api/Api";

// Components
import ListPatients from "./ListPatients";
import Appointment from "./Appointment";
import Box from "@material-ui/core/Box";


toast.configure();

const DoctorSideMenu = ({setAuth, classes}) => {
    const currentUser = useContext(UserContext);
    const [open, setOpen] = useState(false);
    const [index, setIndex] = useState(0);
    const [ med_name, setMedName ] = useState("");


    const logout = async e => {
        e.preventDefault();
        try {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            setAuth(false);
            window.location.replace("/");
            toast.success("Logout successfully!");
        } catch (err){
            console.error(err.message);
        }
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleClickMenuItem = index => {
        setIndex(index);
    };

    const setDefaultIndex = () => {
        if(window.location.pathname === "/appointments"){
            setIndex(1);
        } else if(window.location.pathname === "/notifications"){
            setIndex(2);
        } else {
            setIndex(0);
        }
    }

    useEffect(() => {
        setDefaultIndex();

        const getName = async () => {

            const med_name = await getMedName(currentUser.user_id);

            if(!med_name){
                toast.error("Error occurred");
            }

            return med_name;
        }
        getName().then(res => setMedName(res.med_name));
    }, [currentUser.user_id]);

    return (
        <Fragment>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit" aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}>
                        <MenuIcon />
                    </IconButton>

                    <Box width="100%">
                        <Grid container justify="space-between">
                            <Grid item>
                                <Typography variant="h4" noWrap>Hello, Dr {med_name}! </Typography>
                            </Grid>
                                <Grid item>
                                <Button onClick={e => logout(e)} color="inherit">Logout</Button>
                            </Grid>
                        </Grid>
                    </Box>

                </Toolbar>
            </AppBar>

            <Router>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}>
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <MenuList>
                    <Link className={classes.link} to="/">
                        <MenuItem button selected={index === 0} onClick={() => handleClickMenuItem(0)}>
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <Typography variant="button">
                                <ListItemText disableTypography={true} primary="Main Page" />
                            </Typography>

                        </MenuItem>
                    </Link>
                    <Link className={classes.link} to="/appointments">
                        <MenuItem button selected={index === 1} onClick={() => handleClickMenuItem(1)}>
                            <ListItemIcon>
                                <EventAvailableIcon />
                            </ListItemIcon>
                            <Typography variant="button">
                                <ListItemText disableTypography={true} primary="Appointment" />
                            </Typography>

                        </MenuItem>
                    </Link>
                </MenuList>
            </Drawer>
                <main className={classes.content}>
                <div className={classes.toolbar} />
                <Switch>
                    <UserContext.Provider value={ currentUser }>
                        <Route exact path="/">
                            <Fragment>
                                <ListPatients classes={classes} />
                            </Fragment>

                        </Route>
                        <Route exact path="/appointments">
                            <Appointment user_info={{'name': med_name}} classes={classes} />
                        </Route></UserContext.Provider>
                </Switch>
                </main>
            </Router>
        </Fragment>
    );
}

export default DoctorSideMenu;
