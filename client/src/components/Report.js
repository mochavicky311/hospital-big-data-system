import React, {Fragment, useEffect, useState, useRef} from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Button from "@material-ui/core/Button";
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import moment from "moment";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import GaugeChart from 'react-gauge-chart';
import {Card, Dialog, DialogContent} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import {getAllPatients, getReportData, getTotalBed, updateReportData} from "../api/Api";
import { ResponsiveBar } from '@nivo/bar';
import { ResponsivePie } from '@nivo/pie';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import ReactToPrint from "react-to-print";


toast.configure();

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    }
});


const Report = ({ classes }) => {
    const componentRef = useRef();

    const [ selectedDate, setSelectedDate ] = useState(new Date(new Date().setMonth(new Date().getMonth()-1)));
    const [ reportData, setReportData ] = useState({});
    const [ prevReportData, setPrevReportData ] = useState({});
    const [ dbTotalBed, setDbTotalBed ] = useState(0);
    const [ totalBed, setTotalBed ] = useState(0);
    const [ errorText, setErrorText ] = useState("");
    const [ openPdf, setOpenPdf ] = useState(false);

    const [ gender, setGender ] = useState([]);
    const [ ageGroups, setAgeGroups ] = useState([]);
    const [ race, setRace ] = useState([]);
    const [ disease, setDisease ] = useState([]);

    const [ bedOccupancy, setBedOccupancy ] = useState(0);
    const [ turnoverRate, setTurnoverRate ] = useState(0);
    const [ doctorToPatientRatio, setDoctorToPatientRatio ] = useState(0);
    const [ diffBedOccupancy, setDiffBedOccupancy ] = useState(0);
    const [ diffTurnoverRate, setDiffTurnoverRate ] = useState(0);
    const [ diffDoctorToPatientRatio, setDiffDoctorToPatientRatio ] = useState(0);

    const handleChange = date => {
        setSelectedDate(date);
    };

    const handleClosePdf = () => {
        setOpenPdf(false);
    };

    const handleOpenPdf = () => {
        setOpenPdf(true);
    };


    const handleTotalBedChange = e => {
        let n = e.target.value;

        setTotalBed(n);

        if (n >= 0) {
            setErrorText("");
        }
        else {
            setErrorText("The value cannot be less than 0.");
        }
    };


    const updateBedNo = async e => {
        e.preventDefault();

        if(errorText === ""){

            const parseRes = await updateReportData({
                "total_beds": totalBed
            })

            if(parseRes==="Updated successfully!"){
                toast.success(parseRes);
                setDbTotalBed(totalBed);
            } else {
                toast.error(parseRes);
            }

        } else {
            toast.error("Input error.");
        }
    };


    const generateReport = async e => {
        e.preventDefault();

        const selectedYear = selectedDate.getFullYear();
        const selectedMonth = selectedDate.getMonth();

        const getDate = new Date(selectedYear, selectedMonth + 1, 0);
        const getPrevDate = new Date(selectedYear, selectedMonth, 0);

        const jsonData = await getReportData(selectedYear, selectedMonth+1, getDate.getDate());
        jsonData.date = moment.utc(jsonData.date).toDate();
        setReportData(jsonData);

        const jsonData2 = await getReportData(getPrevDate.getFullYear(), getPrevDate.getMonth()+1, getPrevDate.getDate());
        jsonData2.date = moment.utc(jsonData.date).toDate();
        setPrevReportData(jsonData2);

        const patients = await getAllPatients();
        if (!patients) {
            toast.error("Error occurred");
        } else {
            handleOpenPdf();
        }
    };


    useEffect(() => {

        const getCurrentBeds = async () => {
            const jsonData = await getTotalBed();
            setDbTotalBed(jsonData.total_beds);
        };

        getCurrentBeds().then(() => setTotalBed(dbTotalBed));

    }, [dbTotalBed]);


    useEffect(() => {

        const calculateData = () => {
            const occupancy = Math.round((reportData.current_patients / reportData.total_beds)*100);
            const turnover = Math.round(reportData.total_discharges / reportData.total_beds * 100) / 100;
            const doctorToPatient = Math.round(reportData.current_doctors / reportData.current_patients * 100) / 100;
            const prevOccupancy = Math.round(prevReportData.current_patients / prevReportData.total_beds * 100);
            const prevTurnover  = Math.round(prevReportData.total_discharges / prevReportData.total_beds * 100) / 100;
            const prevDoctorToPatient = Math.round((prevReportData.current_doctors / prevReportData.current_patients) * 100) / 100;

            setBedOccupancy(occupancy);
            setTurnoverRate(turnover);
            setDoctorToPatientRatio(doctorToPatient);
            setDiffBedOccupancy(occupancy - prevOccupancy);
            setDiffTurnoverRate(Math.round((turnover - prevTurnover) * 100) / 100);
            setDiffDoctorToPatientRatio(Math.round((doctorToPatient - prevDoctorToPatient) * 100) / 100);

            // gender
            setGender([
                {
                    "id": "male",
                    "label": "male",
                    "value": reportData.male
                },
                {
                    "id": "female",
                    "label": "female",
                    "value": reportData.female
                },
            ])

            // age groups
            setAgeGroups([
                {
                    "age": "< 18",
                    "count": reportData.agegrp1
                },
                {
                    "age": "18 to 39",
                    "count": reportData.agegrp2
                },
                {
                    "age": "40 to 64",
                    "count": reportData.agegrp3
                },
                {
                    "age": "65+",
                    "count": reportData.agegrp4
                }
            ])

            // race
            setRace([
                {
                    "id": "malay",
                    "label": "Malay",
                    "value": reportData.malay
                },
                {
                    "id": "chinese",
                    "label": "Chinese",
                    "value": reportData.chinese
                },
                {
                    "id": "indian",
                    "label": "Indian",
                    "value": reportData.indian
                },
                {
                    "id": "others",
                    "label": "Others",
                    "value": reportData.other
                }
            ])

            // disease
            setDisease([
                {
                    "disease": "coronary heart disease",
                    "count": reportData.disease1
                },
                {
                    "disease": "arrhythmia",
                    "count": reportData.disease2
                },
                {
                    "disease": "cardiomyopathy",
                    "count": reportData.disease3
                },
                {
                    "disease": "heart failure",
                    "count": reportData.disease4
                },
                {
                    "disease": "heart infections & inflammation",
                    "count": reportData.disease5
                },
                {
                    "disease": "heart valve disease",
                    "count": reportData.disease6
                }
            ])

        };

        calculateData();

    }, [reportData, prevReportData]);


    return (
        <Fragment>
            <Box marginY={3}>
                <Typography variant="h3" gutterBottom>Report Management</Typography>
            </Box>

            <Box width="100%">
                <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Box marginTop={2}>
                        <Typography variant="h5">Update No. of Beds</Typography>
                        <Typography variant="caption" gutterBottom>(current: {dbTotalBed})</Typography>
                    </Box>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item>
                        <TextField
                            type="number"
                            variant="outlined"
                            label="Current Bo. of Beds"
                            value={totalBed}
                            onChange={handleTotalBedChange}
                            InputProps={{
                                inputProps: {min: 0, step: 1}
                            }}
                            required
                            error={errorText !== ""}
                            helperText={errorText}/>
                    </Grid>
                    <Grid item>
                        <Button onClick={updateBedNo} color="primary" variant="contained">Update</Button>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                        <Box marginTop={3}>
                            <Typography variant="h5" gutterBottom>Monthly Report</Typography>
                        </Box>
                    </Grid>
                <Grid container spacing={2}>
                        <Grid item>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    disableToolbar
                                    label="Report Year and Month"
                                    inputVariant="outlined"
                                    openTo="year"
                                    views={["year", "month"]}
                                    minDate={new Date("2020-01-01")}
                                    maxDate={new Date(new Date().setMonth(new Date().getMonth()-1))}
                                    value={selectedDate}
                                    onChange={handleChange}
                                    disableFuture
                                    required
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item>
                            <Button onClick={generateReport} color="primary" variant="contained">Generate Report</Button>
                        </Grid>
                    </Grid>
            </Grid>
            </Box>

            <Dialog open={openPdf} onClose={handleClosePdf} maxWidth="xl" fullWidth>
                <DialogContent>
                    <Box marginTop={2} marginRight={5}>
                        <Grid container spacing={2} justify="flex-end">
                            <Grid item>
                                <ReactToPrint
                                    trigger={() => <Button color="primary" variant="contained">Save PDF</Button>}
                                    content={() => componentRef.current}
                                    documentTitle={`Report${selectedDate.getFullYear()}_${selectedDate.getMonth()+1}.pdf`}
                                />
                            </Grid>
                            <Grid item>
                                <Button onClick={handleClosePdf} color="primary" variant="contained">Cancel</Button>
                            </Grid>
                        </Grid>
                    </Box>

                    <Document ref={componentRef}>
                        <Box marginX={5}>
                            <Page size="A4" style={styles.page}>
                                <View style={styles.section}>
                                    <Text>
                                        <Box marginBottom={3}>
                                            <Typography variant="h2">
                                                Monthly Report ({moment(selectedDate).format('MMMM YYYY')})
                                            </Typography>
                                            <Typography variant="body1" color="textSecondary" gutterBottom>(∆ prev. month)</Typography>
                                        </Box>
                                        <Grid container spacing={3} justify="space-between">
                                            <Grid item xs={6}>
                                                <Card className={classes.whiteDashboard}>
                                                    <CardContent>
                                                        <Box marginBottom={2}>
                                                            <Typography variant="h4" gutterBottom align='center'>Bed Occupancy Rate</Typography>
                                                        </Box>
                                                        <GaugeChart id="occupancy"
                                                                    nrOfLevels={3}
                                                                    arcsLength={[0.3, 0.5, 0.2]}
                                                                    colors={['#CCEBC4', '#fbf1b0', '#FBB4B0']}
                                                                    percent={reportData.current_patients / reportData.total_beds}
                                                                    hideText={true}
                                                        />
                                                        <Box align="center">
                                                            <Typography variant="h2" display="inline">
                                                                {bedOccupancy}%
                                                            </Typography>
                                                            <Typography variant="h4" color="textSecondary" display="inline">
                                                                {' '}({diffBedOccupancy}%)
                                                            </Typography>
                                                        </Box>

                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Card className={classes.whiteDashboard}>
                                                    <CardContent>
                                                        <Typography variant="h4" gutterBottom align='center'>Patient Turnover Rate</Typography>
                                                        <Box marginBottom={5} align="center">
                                                            <Typography variant="h2" display="inline">
                                                                {turnoverRate}
                                                            </Typography>
                                                            <Typography variant="h4" display="inline" color="textSecondary">
                                                                {' '}({diffTurnoverRate})
                                                            </Typography>
                                                        </Box>
                                                        <Typography variant="h4" gutterBottom align='center'>Doctor to Patient Ratio</Typography>
                                                        <Box align="center">
                                                            <Typography variant="h2" display="inline">
                                                                {doctorToPatientRatio}
                                                            </Typography>
                                                            <Typography variant="h4" color="textSecondary" display="inline">
                                                                {' '}({diffDoctorToPatientRatio})
                                                            </Typography>
                                                        </Box>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Card>
                                                    <CardContent>
                                                        <Typography variant="h5" gutterBottom align='center'>No. of Beds</Typography>
                                                        <Box align="center">
                                                            <Typography variant="h3" display="inline">
                                                                {reportData.total_beds}
                                                            </Typography>
                                                            <Typography variant="h4" color="textSecondary" display="inline">
                                                                {' '}({reportData.total_beds - prevReportData.total_beds})
                                                            </Typography>
                                                        </Box>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Card>
                                                    <CardContent>
                                                        <Typography variant="h5" gutterBottom align='center'>Current Patients</Typography>
                                                        <Box align="center">
                                                            <Typography variant="h3" display="inline">
                                                                {reportData.current_patients}
                                                            </Typography>
                                                            <Typography variant="h4" color="textSecondary" display="inline">
                                                                {' '}({reportData.current_patients - prevReportData.current_patients})
                                                            </Typography>
                                                        </Box>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Card>
                                                    <CardContent>
                                                        <Typography variant="h5" gutterBottom align='center'>Current Doctors</Typography>
                                                        <Box align="center">
                                                            <Typography variant="h3" display="inline">
                                                                {reportData.current_doctors}
                                                            </Typography>
                                                            <Typography variant="h4" color="textSecondary" display="inline">
                                                                {' '}({reportData.current_doctors - prevReportData.current_doctors})
                                                            </Typography>
                                                        </Box>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Card className={classes.whiteDashboard}>
                                                    <CardContent>
                                                        <Typography variant="h4" gutterBottom align='center'>Patient Demography</Typography>
                                                        <Grid container spacing={2}>
                                                            <Grid item xs={6}>
                                                                <Typography variant="h5" gutterBottom align='center'>Gender</Typography>
                                                                <Box height="300px">
                                                                    <ResponsivePie
                                                                        data={gender}
                                                                        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                                                                        innerRadius={0.5}
                                                                        padAngle={0.7}
                                                                        cornerRadius={3}
                                                                        activeOuterRadiusOffset={8}
                                                                        colors={{ "scheme": "pastel1" }}
                                                                        borderWidth={1}
                                                                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                                                                        arcLinkLabelsSkipAngle={10}
                                                                        arcLinkLabelsTextColor="#333333"
                                                                        arcLinkLabelsThickness={2}
                                                                        arcLinkLabelsColor={{ from: 'color' }}
                                                                        arcLabelsSkipAngle={10}
                                                                        arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                                                                        legends={[
                                                                            {
                                                                                anchor: 'bottom',
                                                                                direction: 'row',
                                                                                justify: false,
                                                                                translateX: 0,
                                                                                translateY: 56,
                                                                                itemsSpacing: 0,
                                                                                itemWidth: 100,
                                                                                itemHeight: 18,
                                                                                itemTextColor: '#999',
                                                                                itemDirection: 'left-to-right',
                                                                                itemOpacity: 1,
                                                                                symbolSize: 18,
                                                                                symbolShape: 'circle',
                                                                                effects: [
                                                                                    {
                                                                                        on: 'hover',
                                                                                        style: {
                                                                                            itemTextColor: '#000'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        ]}
                                                                    />
                                                                </Box>
                                                            </Grid>
                                                            <Grid item xs={6}>
                                                                <Typography variant="h5" gutterBottom align='center'>Age</Typography>
                                                                <Box height="300px">
                                                                    <ResponsiveBar
                                                                        data={ageGroups}
                                                                        keys={[ 'count']}
                                                                        indexBy="age"
                                                                        margin={{ top: 50, right: 60, bottom: 50, left: 60 }}
                                                                        padding={0.3}
                                                                        valueScale={{ type: 'linear' }}
                                                                        indexScale={{ type: 'band', round: true }}
                                                                        colors={{ "scheme": "pastel1" }}
                                                                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                                                                        axisTop={null}
                                                                        axisRight={null}
                                                                        axisBottom={{
                                                                            tickSize: 5,
                                                                            tickPadding: 5,
                                                                            tickRotation: 0,
                                                                            legend: 'Age Groups',
                                                                            legendPosition: 'middle',
                                                                            legendOffset: 32
                                                                        }}
                                                                        axisLeft={{
                                                                            tickSize: 5,
                                                                            tickPadding: 5,
                                                                            tickRotation: 0,
                                                                            legend: 'Number of patients',
                                                                            legendPosition: 'middle',
                                                                            legendOffset: -40
                                                                        }}
                                                                        labelSkipWidth={12}
                                                                        labelSkipHeight={12}
                                                                        labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                                                                        animate={true}
                                                                        motionStiffness={90}
                                                                        motionDamping={15}
                                                                    />
                                                                </Box>
                                                            </Grid>
                                                            <Grid item xs={6}>
                                                                <Typography variant="h5" gutterBottom align='center'>Heart Disease</Typography>
                                                                <Box height="300px">
                                                                    <ResponsiveBar
                                                                        data={disease}
                                                                        keys={[ 'count']}
                                                                        indexBy="disease"
                                                                        margin={{ top: 40, right: 60, bottom: 120, left: 60 }}
                                                                        padding={0.3}
                                                                        valueScale={{ type: 'linear' }}
                                                                        indexScale={{ type: 'band', round: true }}
                                                                        colors={{ "scheme": "pastel2" }}
                                                                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                                                                        axisTop={null}
                                                                        axisRight={null}
                                                                        axisBottom={{
                                                                            tickSize: 5,
                                                                            tickPadding: 5,
                                                                            tickRotation: -40,
                                                                            legend: 'Disease',
                                                                            legendPosition: 'middle',
                                                                            legendOffset: 60
                                                                        }}
                                                                        axisLeft={{
                                                                            tickSize: 5,
                                                                            tickPadding: 5,
                                                                            tickRotation: 0,
                                                                            legend: 'Number of patients',
                                                                            legendPosition: 'middle',
                                                                            legendOffset: -30
                                                                        }}
                                                                        labelSkipWidth={12}
                                                                        labelSkipHeight={12}
                                                                        labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                                                                        animate={true}
                                                                        motionStiffness={90}
                                                                        motionDamping={15}
                                                                    />
                                                                </Box>
                                                            </Grid>
                                                            <Grid item xs={6}>
                                                                <Typography variant="h5" gutterBottom align='center'>Race</Typography>
                                                                <Box height="300px">
                                                                    <ResponsivePie
                                                                        data={race}
                                                                        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                                                                        innerRadius={0.5}
                                                                        padAngle={0.7}
                                                                        cornerRadius={3}
                                                                        activeOuterRadiusOffset={8}
                                                                        borderWidth={1}
                                                                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                                                                        arcLinkLabelsSkipAngle={10}
                                                                        arcLinkLabelsTextColor="#333333"
                                                                        arcLinkLabelsThickness={2}
                                                                        arcLinkLabelsColor={{ from: 'color' }}
                                                                        arcLabelsSkipAngle={10}
                                                                        arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                                                                        colors={{ "scheme": "pastel1" }}
                                                                        legends={[
                                                                            {
                                                                                anchor: 'bottom',
                                                                                direction: 'row',
                                                                                justify: false,
                                                                                translateX: 0,
                                                                                translateY: 56,
                                                                                itemsSpacing: 0,
                                                                                itemWidth: 100,
                                                                                itemHeight: 18,
                                                                                itemTextColor: '#999',
                                                                                itemDirection: 'left-to-right',
                                                                                itemOpacity: 1,
                                                                                symbolSize: 18,
                                                                                symbolShape: 'circle',
                                                                                effects: [
                                                                                    {
                                                                                        on: 'hover',
                                                                                        style: {
                                                                                            itemTextColor: '#000'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        ]}
                                                                    />
                                                                </Box>

                                                            </Grid>
                                                        </Grid>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                        </Grid>
                                    </Text>
                                </View>
                            </Page>
                        </Box>

                    </Document>

                </DialogContent>

            </Dialog>

        </Fragment>
    );
}

export default Report;