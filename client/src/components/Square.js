import React, { Fragment } from "react";


const Square = ({classes, color}) => {

    return (

        <Fragment>
            <div className={classes.square} style={{ backgroundColor: color }}/>
        </Fragment>
    );
}

export default Square;