import React, { Fragment, useRef, useState } from "react";
import {toast} from "react-toastify";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Box from "@material-ui/core/Box";

toast.configure();

const FileUpload = ({patient_id, type}) => {

    const [file, setFile] = useState(''); // storing the uploaded file
    const el = useRef(); // accesing input element

    const handleChange = (e) => {
        const file = e.target.files[0];
        setFile(file);
    }

    const uploadFile = async e=> {
        e.preventDefault();

        let url;

        if (file !== ''){
            if (type === "ecg"){
                url = `http://localhost:5000/upload/ecg/${patient_id}`;
            } else if (type === "heartsound") {
                url = `http://localhost:5000/upload/heartsound/${patient_id}`;
            } else {
                url = `http://localhost:5000/upload/habit/${patient_id}`;
            }
            const formData = new FormData();
            formData.append('file', file);

            axios.post(url, formData)
                .then(res => {
                console.log(res);
                if(res.status===500){
                    toast.error(res.data);
                } else {
                    toast.success(res.data);
                }

            }).catch(
                err => console.error(err)
            )

        } else {
            toast.error("Please select file.");
        }
    }

    return (
        <Fragment>
            <form>
                {type==="heartsound" ? <input type="file" accept=".wav" ref={el} onChange={handleChange} />
                : <input type="file" accept=".csv" ref={el} onChange={handleChange} />}
                <Box marginY={3}>
                    <Button onClick={uploadFile} color="primary" variant="contained" size="small">Upload</Button>
                </Box>

            </form>

        </Fragment>
    );

}

export default FileUpload;