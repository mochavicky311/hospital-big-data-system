import {toast} from "react-toastify";

// doctor
export const updateDoctor = async (med_id, body) => {
    try {
        const response = await fetch(`http://localhost:5000/doctor/${med_id}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const registerDoctor = async body => {
    try {
        const response = await fetch("http://localhost:5000/auth/registerDoctor", {
            method: "POST",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch(err) {
        console.error(err.message)
    }
}


export const getMedName = async (med_id) => {
    try {
        const response = await fetch(`http://localhost:5000/doctor/${med_id}`, {
            method: "GET",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const getAllDoctors = async () => {

    try {
        const response = await fetch("http://localhost:5000/doctor/", {
            method: "GET",
            headers: { token: localStorage.token }
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const deleteADoctor = async (med_id) => {
    try {
        const response = await fetch(`http://localhost:5000/doctor/${med_id}`, {
            method: "DELETE",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


// patients
export const dischargeOrAdmitPatient = async (id, value) => {
    try {
        const response = await fetch(`http://localhost:5000/patients/discharge/${id}/${value}`, {
            method: "PUT",
            headers: {token: localStorage.token}
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const updatePatient = async (patient_id, body) => {
    try {
        const response = await fetch(`http://localhost:5000/patients/${patient_id}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const getAllPatients = async () => {
    try {

        const response = await fetch("http://localhost:5000/patients/", {
            method: "GET",
            headers: {token: localStorage.token}
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const getPatientsUnderDoctor = async (med_id) => {
    try {
        const response = await fetch(`http://localhost:5000/doctor/${med_id}/patients`, {
            method: "GET",
            headers: {token: localStorage.token}
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const registerPatient = async body => {
    try {
        const response = await fetch("http://localhost:5000/auth/registerPatient", {
            method: "POST",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch(err) {
        console.error(err.message)
    }
}


export const getAPatient = async (patient_id) => {
    try {
        const response = await fetch(`http://localhost:5000/patients/${patient_id}`, {
            method: "GET",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


// nurse
export const getAllNurses = async () => {
    try {
        const response = await fetch("http://localhost:5000/nurse/", {
            method: "GET",
            headers: {token: localStorage.token}
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const deleteANurse = async (nurse_id) => {
    try {
        const response = await fetch(`http://localhost:5000/nurse/${nurse_id}`, {
            method: "DELETE",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const registerNurse = async (body) => {
    try {

        const response = await fetch("http://localhost:5000/auth/registerNurse", {
            method: "POST",
            headers: {"Content-Type": "application/json", token: localStorage.token},
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch (err){
        console.error(err);
    }
}


export const updateNurse = async (nurse_id, body) => {
    try {
        const response = await fetch(`http://localhost:5000/nurse/${nurse_id}`, {
            method: "PUT",
            headers: {"Content-Type": "application/json", token: localStorage.token},
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch (err) {
        console.error(err);
    }
}


// update report data (multiple keys)
export const updateReportData = async body => {
    console.log("inside update report data2:", body);
    try {
        const response = await fetch(`http://localhost:5000/report`, {
            method: "PUT",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(body)
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const getReportData = async (year, month, date) => {
    try {
        const response = await fetch(`http://localhost:5000/report/${year}/${month}/${date}`, {
            method: "GET",
            headers: { token: localStorage.token }
        })
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const getTotalBed = async () => {
    try {

        const response = await fetch("http://localhost:5000/report/bed", {
            method: "GET",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err);
    }
}


// user
export const getAUser = async (user_id) => {
    try {
        const response = await fetch(`http://localhost:5000/users/${user_id}`, {
            method: "GET",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


// appointments
export const getAppointments = async (year, month, day, id, c) => {
    try {
        const response = await fetch(`http://localhost:5000/appointment/${c}/${id}/${year}/${month}/${day}`, {
            method: "GET",
            headers: {token: localStorage.token}
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const createNewAppointments = async (newApp) => {
    try {
        const response = await fetch(`http://localhost:5000/appointment`, {
            method: "POST",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(newApp)
        });

        const parseRes =  await response.json();

        if(parseRes.message==="Created successfully!"){
            toast.success(parseRes);
            return parseRes.appointment_id;
        } else {
            toast.error(parseRes);
            return false;
        }

    } catch (err) {
        console.error(err.message);
        return false;
    }
}


export const deleteAppointment = async (id) => {
    try {
        const response = await fetch(`http://localhost:5000/appointment/${id}`, {
            method: "DELETE",
            headers: { token: localStorage.token }
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


export const modifyAppointment = async (appId, modifiedColumns) => {
    try {
        const response = await fetch(`http://localhost:5000/appointment/${appId}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json", token: localStorage.token },
            body: JSON.stringify(modifiedColumns)
        });

        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}


// diseases
export const getAllDiseases = async () => {
    try {
        const response = await fetch("http://localhost:5000/diseases", {
            method: "GET",
            headers: {token: localStorage.token}
        });
        return await response.json();

    } catch (err) {
        console.error(err.message);
    }
}