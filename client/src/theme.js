import { createMuiTheme } from '@material-ui/core/styles';
import PlayfairDisplay from './fonts/PlayfairDisplay-VariableFont_wght.ttf';


const playfairDisplay = {
    fontFamily: 'PlayfairDisplay',
    fontDisplay: 'swap',
    src: `
   url(${PlayfairDisplay}) format('truetype')
 `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};


const theme = createMuiTheme({
    typography: {
        fontFamily: ['PlayfairDisplay', 'Roboto', 'sans-serif'].join(','),
        h1: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 400,
            fontSize: 70,
            letterSpacing: -1.5,
            color: "#436170"
        },
        h2: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 400,
            fontSize: 48,
            letterSpacing: -0.5
        },
        h3: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 600,
            fontSize: 34,
            letterSpacing: 0
        },
        h4: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 500,
            fontSize: 24,
            letterSpacing: 0.25
        },
        h5: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 500,
            fontSize: 20,
            letterSpacing: 0
        },
        h6: {
            fontFamily: 'PlayfairDisplay',
            fontWeight: 500,
            fontSize: 16,
            letterSpacing: 0.15
        },
        body1: {
            fontFamily: 'Roboto',
            fontWeight: 400,
            fontSize: 16,
            letterSpacing: 0.5
        },
        body2: {
            fontFamily: 'Roboto',
            fontWeight: 400,
            fontSize: 14,
            letterSpacing: 0.25
        },
        button: {
            fontFamily: 'Roboto',
            fontWeight: 500,
            fontSize: 14,
            letterSpacing: 1.25
        },
        caption: {
            fontFamily: 'Roboto',
            fontWeight: 400,
            fontSize: 12,
            letterSpacing: 0.4
        }
    },
    palette: {
        primary: {
            main: "#2F5061"
        },
        secondary: {
            main: "#f76c6c"
        },
        divider: "#ffffff",
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                '@font-face': [playfairDisplay],
            },
        }
    }
})

export default theme;
