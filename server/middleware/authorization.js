/*
* To check whether the token is valid *
 */

const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = async (req, res, next) => {
    try {

        const jwtToken = req.header("token");

        if(!jwtToken) {
            return res.status(403).json("Not Authorized");
        }

        //If verified token, will return the payload.
        const payload = jwt.verify(jwtToken, process.env.jwtSecret);

        req.user_id = payload.user_id;


    } catch (err) {
        console.error(err.message);
        return res.status(403).json("Not Authorized");
    }

    next();
}