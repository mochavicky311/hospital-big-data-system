const admin = require('firebase-admin')

// Initialize firebase admin SDK
admin.initializeApp({
    credential: admin.credential.cert(`${__dirname}\\..\\hospital-big-data-system-firebase-adminsdk-6msms-e7be3a50d9.json`),
    storageBucket: "hospital-big-data-system.appspot.com"
});

const bucket = admin.storage().bucket();

module.exports = {
    bucket
}