const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = async (req, res, next) => {

    if(req.path === "/registerPatient"){
        const { patient_name,
            patient_email,
            patient_password,
            patient_contact,
            med_id,
            patient_gender,
            patient_birth_year,
            patient_emgcy_no,
            patient_height_m,
            patient_weight_kg,
            patient_bmi,
            disease,
            is_vegan,
            patient_race,
            is_married,
            in_patient } = req.body;

        if(![patient_name, patient_email, patient_password, patient_contact, med_id, patient_gender,
            patient_birth_year, patient_emgcy_no, patient_height_m, patient_weight_kg, patient_bmi, disease,
            is_vegan.toString(), patient_race, is_married.toString(), in_patient.toString()].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    if(req.baseUrl === "/patients"){

        const { patient_name,
            patient_email,
            patient_password,
            patient_contact,
            med_id,
            patient_gender,
            patient_birth_year,
            patient_emgcy_no,
            disease,
            is_vegan,
            patient_race,
            is_married } = req.body;

        if(![patient_name, patient_email, patient_password, patient_contact, med_id, patient_gender,
            patient_birth_year, patient_emgcy_no, disease, is_vegan.toString(), patient_race, is_married.toString()]
            .every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    else if(req.path === "/registerDoctor" || req.baseUrl === "/doctor") {
        const { med_name, med_email, med_password, med_contact } = req.body;

        if(![med_name, med_email, med_password, med_contact].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    else if(req.path === "/registerAdmin" || req.baseUrl === "/admin"){
        const { admin_name, admin_email, admin_password } = req.body;

        if(![admin_name, admin_email, admin_password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    else if (req.path === "/registerNurse" || req.baseUrl === "/nurse") {
        const { nurse_name, nurse_email, nurse_password } = req.body;

        if(![nurse_name, nurse_email, nurse_password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    else if (req.path === "/loginPatient" || req.path === "/loginDoctor" || req.path === "/loginAdmin" || req.path === "/loginNurse") {
        const { email, password } = req.body;

        if(![email, password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    }

    next();
};