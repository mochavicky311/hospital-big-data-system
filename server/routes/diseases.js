const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");


//get all diseases
router.get("/", authorization, async(req, res) => {
    try {
        const allDiseases = await pool.query("SELECT * FROM diseases");
        res.json(allDiseases.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;