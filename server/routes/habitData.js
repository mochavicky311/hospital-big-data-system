const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const habitAnalyzer = require("../utils/habitAnalyzer");


//get recent habit data for a patient
router.get("/:id/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { id, year, month, day }  = req.params;
        const allHabitData = await pool.query(`SELECT * FROM habit_data 
                                            WHERE patient_id='${ id }'
                                            AND record_date BETWEEN (date '${year}-${month}-${day}' - integer '7') AND (date '${year}-${month}-${day}' + integer '1')
                                            ORDER BY record_date`);

        for(let i=1; i<allHabitData.rows.length; i++){
            // get habit analytics
            const sleep_result = habitAnalyzer(allHabitData.rows[i].sleep_change,
                allHabitData.rows[i-1].sleep_classification,
                allHabitData.rows[i].sleep_classification, "sleep");
            const diet_result = habitAnalyzer(allHabitData.rows[i].diet_change,
                allHabitData.rows[i-1].diet_classification,
                allHabitData.rows[i].diet_classification, "diet");
            const exercise_result = habitAnalyzer(allHabitData.rows[i].exercise_change,
                allHabitData.rows[i-1].exercise_classification,
                allHabitData.rows[i].exercise_classification,"exercise");
            const overall_result = habitAnalyzer(allHabitData.rows[i].overall_change,
                allHabitData.rows[i-1].overall_change,
                allHabitData.rows[i].overall_change,"overall");

            const habit_analytics = {
                "sleep_cat": sleep_result.category,
                "sleep_analytics": sleep_result.analytics,
                "diet_cat": diet_result.category,
                "diet_analytics": diet_result.analytics,
                "exercise_cat": exercise_result.category,
                "exercise_analytics": exercise_result.analytics,
                "overall_cat": overall_result.category,
                "overall_analytics": overall_result.analytics
            };

            allHabitData.rows[i]["habit_analytics"] = habit_analytics;
        }

        res.json(allHabitData.rows.slice(1));
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;