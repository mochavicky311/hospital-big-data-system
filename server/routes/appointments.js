const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");


//create new appointment
router.post("/", authorization, async(req, res) => {
    try {

        const { patient,
            doctor,
            startDate,
            endDate,
            notes,
            title }  = req.body;

        let validNotes = '';
        if(typeof notes === 'string') validNotes = notes;

        const appointment_id = await pool.query(
            "INSERT INTO appointments " +
            "(patient, doctor, \"startDate\", \"endDate\", notes, title) " +
            `VALUES ('${patient}', '${doctor}', '${startDate}', '${endDate}', '${validNotes}', '${title}') 
            RETURNING id;`
        );

        res.json({
            message: "Created successfully!",
            appointment_id: appointment_id
        });

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all appointments on a date of a patient
router.get("/p1/:year/:month/:day/:patient_id", authorization, async(req, res) => {
    try {
        const { year, month, day, patient_id }  = req.params;

        const appointments = await pool.query(`SELECT * FROM appointments
                                            WHERE patient='${ patient_id }'
                                            AND ("startDate"::date=date '${year}-${month}-${day}'
                                            OR "endDate"::date=date '${year}-${month}-${day}')`);

        res.json(appointments.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all appointments on a date of a doctor
router.get("/d1/:year/:month/:day/:med_id", authorization, async(req, res) => {
    try {
        const { year, month, day, med_id }  = req.params;

        const appointments = await pool.query(`SELECT * FROM appointments
                                            WHERE doctor='${ med_id }'
                                            AND ("startDate"::date=date '${year}-${month}-${day}'
                                            OR "endDate"::date=date '${year}-${month}-${day}')`);

        res.json(appointments.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all appointments of a doctor of recent 13 days. (today + 6 days before + 6 days after)
router.get("/d/:med_id/:year/:month/:day/", authorization, async(req, res) => {
    try {
        const { year, month, day, med_id }  = req.params;

        const appointments = await pool.query(`SELECT * FROM appointments 
                                            WHERE doctor='${ med_id }'
                                            AND "startDate" BETWEEN (date '${year}-${month}-${day}' - integer '6') 
                                            AND (date '${year}-${month}-${day}' + integer '7')`);

        res.json(appointments.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all appointments of a patient of recent 13 days. (today + 6 days before + 6 days after)
router.get("/p/:patient_id/:year/:month/:day/", authorization, async(req, res) => {
    try {
        const { year, month, day, patient_id }  = req.params;
        const appointments = await pool.query(`SELECT * FROM appointments 
                                            WHERE "startDate" BETWEEN (date '${year}-${month}-${day}' - integer '6') 
                                            AND (date '${year}-${month}-${day}' + integer '7')
                                            AND patient = '${patient_id}'`);

        res.json(appointments.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//create new appointment
router.post("/", authorization, async(req, res) => {
    try {
        const { id,
            patient,
            doctor,
            startDate,
            endDate,
            notes,
            title }  = req.body;

        await pool.query(
            "INSERT INTO appointments " +
            "(id, patient, doctor, \"startDate\", \"endDate\", notes, title) " +
            `VALUES ('${id}', '${patient}', ${doctor}, ${startDate}, ${endDate}, ${notes}, '${title}');`
        );

        res.json("Created successfully!");

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//delete an appointment
router.delete("/:id", authorization, async (req, res) => {
    try {
        const { id } = req.params;
        await pool.query(`DELETE FROM appointments WHERE id='${id}'`);

        res.json("Deleted successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update an appointment
router.put("/:id", authorization, async(req, res) => {
    try {
        const { id } = req.params;

        await Object.keys(req.body).forEach(
            key => pool.query(`UPDATE appointments SET "${key}" = '${req.body[key]}' WHERE id='${id}'`)
        );

        res.json("Updated successfully!");

    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;