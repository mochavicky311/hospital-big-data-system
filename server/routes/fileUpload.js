const router = require("express").Router();
const pool = require("../db");
const fileUpload = require('express-fileupload');
const moment = require('moment');
const axios = require('axios');
const FormData = require('form-data');
const fs = require('fs');
const firebase = require('../middleware/firebase');


router.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 }
}));


// file upload api for ecg
router.post('/ecg/:id', async (req, res) => {

    if (!req.files) {
        res.status(500).json("Server Error");

    } else {
        const myFile = req.files.file;
        const { id } = req.params;
        const filename = `${new Date().getTime()}_${myFile.name}`;
        const path = `${__dirname}\\..\\public\\${filename}`;

        //  mv() method places the file inside public directory
        await myFile.mv(path, async function (err) {
            if (err) {
                console.log(err)
                res.status(500).json("Server Error");
            }

            // upload file to the bucket
            await firebase.bucket.upload(path);

            const file = firebase.bucket.file(filename);
            file.makePublic().catch(err => console.error(err));

            const publicUrl = file.publicUrl();

            let data = new FormData();
            data.append("file", fs.createReadStream(path));


            // get classification
            const config = {
                method: 'post',
                url: 'http://localhost:8000/classify_stress',
                headers: {
                    ...data.getHeaders()
                },
                data : data
            };

            await axios(config).then(async res => {

                const result = res.data['class'];

                await pool.query("INSERT INTO ecg (patient_id, record_datetime, is_stressed, csv_filepath, csv_filename) " +
                    `VALUES('${id}', '${moment().format('YYYY-MM-DD HH:m:s')}', '${result==="Stress"}', '${publicUrl}', '${filename}');`);

            }).catch(err => {
                console.error(err);
            })
        });

        res.json("Updated successfully!");
    }
})


// file upload api for heartsound
router.post('/heartsound/:id', async (req, res) => {

    if (!req.files) {
        res.status(500).json("Server Error");

    } else {
        const myFile = req.files.file;
        const { id } = req.params;
        const filename = `${new Date().getTime()}_${myFile.name}`;
        const path = `${__dirname}\\..\\public\\${filename}`;

        //  mv() method places the file inside public directory
        await myFile.mv(path, async function (err) {
            if (err) {
                console.log(err)
                res.status(500).json("Server Error");
            }

            // upload file to the bucket
            await firebase.bucket.upload(path);

            const file = firebase.bucket.file(filename);
            file.makePublic().catch(err => console.error(err));

            const publicUrl = file.publicUrl();

            let data = new FormData();
            data.append("file", fs.createReadStream(path));


            // get classification
            const config = {
                method: 'post',
                url: 'http://localhost:8000/classify_heartsound',
                headers: {
                    'Content-Type': 'multipart/form-data',
                    ...data.getHeaders()
                },
                data : data
            };

            await axios(config).then(async res => {
                const result = res.data['class'];

                await pool.query("INSERT INTO heartbeat_sound " +
                    "(patient_id, record_datetime, heartsound_path, heartsound_class, filename) " +
                    `VALUES('${id}', '${moment().format('YYYY-MM-DD HH:m:s')}', '${publicUrl}', '${result}', '${filename}');`);

            }).catch(err => {
                console.error(err);
                res.status(500).json("Server Error");
            })
        });

        res.json("Updated successfully!");
    }
})


// file upload api for habit
router.post('/habit/:id', async (req, res) => {

    if (!req.files) {
        res.status(500).json("Server Error");

    } else {
        const myFile = req.files.file;
        const { id } = req.params;
        const filename = `${new Date().getTime()}_${myFile.name}`;
        const path = `${__dirname}\\..\\public\\${filename}`;

        //  mv() method places the file inside public directory
        await myFile.mv(path, async function (err) {
            if (err) {
                console.log(err)
                res.status(500).json("Server Error");
            }

            // upload file to the bucket
            await firebase.bucket.upload(path);

            const file = firebase.bucket.file(filename);
            file.makePublic().catch(err => console.error(err));

            const publicUrl = file.publicUrl();

            let data = new FormData();
            data.append("file", fs.createReadStream(path));

            // get previous data
            const habit = await pool.query(`SELECT * FROM habit_data WHERE patient_id='${id}'` +
                "ORDER BY record_date DESC;")
            const last_habit = habit.rows[0]

            // get classification
            const config = {
                method: 'post',
                url: 'http://localhost:8000/classify_habit',
                headers: {
                    ...data.getHeaders()
                },
                data : data
            };

            await axios(config).then(async res => {

                const { sleep, diet, exercise } = res.data;
                let diet_change = 0;
                let exercise_change = 0;
                let sleep_change = 0;
                let overall_change = 0;

                if(typeof last_habit !== "undefined"){
                    diet_change = diet - last_habit.diet_classification;
                    if(diet_change === 2){
                        // very high change
                        diet_change = 4;
                    } else if(diet_change === -2){
                        // very high change
                        diet_change = -4;
                    } else if(diet_change === 1){
                        // medium change
                        diet_change = 2;
                    } else if(diet_change === -1){
                        //medium change
                        diet_change = -2;
                }

                    exercise_change = exercise - last_habit.exercise_classification;
                    if(exercise_change === 5){
                        exercise_change = 4;
                    } else if(diet_change === -5) {
                        exercise_change = -4;
                    }

                    sleep_change = sleep - last_habit.sleep_classification;

                    if (diet_change === 0 && exercise_change === 0 && sleep_change === 0){
                        overall_change = 0
                    } else {
                        overall_change = Math.round((diet_change + exercise_change + sleep_change) / 3);
                    }
                }

                await pool.query("INSERT INTO habit_data (patient_id, record_date, csv_filepath, csv_filename, " +
                    "sleep_classification, diet_classification, exercise_classification, " +
                    "diet_change, exercise_change, sleep_change, overall_change) " +
                    `VALUES('${id}', '${moment().format('YYYY-MM-DD')}', '${publicUrl}', '${filename}', 
                    ${sleep}, ${diet}, ${exercise}, ${diet_change}, ${exercise_change}, ${sleep_change}, ${overall_change});`);

            }).catch(err => {
                console.error(err);
            })
        });

        res.json("Updated successfully!");
    }
})

module.exports = router;
