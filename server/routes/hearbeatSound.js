const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const firebase = require("../middleware/firebase");


//get heartbeat sound of a patient
router.get("/:id/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { id, year, month, day }  = req.params;
        const allHeartsounds = await pool.query(`SELECT * FROM heartbeat_sound 
                                            WHERE patient_id='${ id }'
                                            AND record_datetime BETWEEN (date '${year}-${month}-${day}' - integer '6') AND (date '${year}-${month}-${day}' + integer '1')
                                            ORDER BY record_datetime`);
        res.json(allHeartsounds.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//delete an heartbeat sound record
router.delete("/:id", authorization, async (req, res) => {
    try {
        const { id } = req.params;
        const record = await pool.query(`DELETE FROM heartbeat_sound WHERE heartbeat_sound_id='${id}' RETURNING filename;`);
        const filename = record.rows[0].filename;

        const file = firebase.bucket.file(filename);
        file.delete().catch(err => console.log(err));

        res.json("Deleted successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});

module.exports = router;