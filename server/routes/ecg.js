const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const firebase = require("../middleware/firebase");
const csv = require("csvtojson");


//get ecg data for a patient
router.get("/:id/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { id, year, month, day }  = req.params;
        const allECG = await pool.query(`SELECT * FROM ecg 
                                            WHERE patient_id='${ id }'
                                            AND record_datetime BETWEEN (date '${year}-${month}-${day}' - integer '6') AND (date '${year}-${month}-${day}' + integer '1')
                                            ORDER BY record_datetime`);
        res.json(allECG.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//delete an ECG record
router.delete("/:id", authorization, async (req, res) => {
    try {
        const { id } = req.params;
        const record = await pool.query(`DELETE FROM ecg WHERE ecg_id='${id}' RETURNING csv_filename`);
        const filename = record.rows[0].csv_filename;

        const file = firebase.bucket.file(filename);
        file.delete().catch(err => console.log(err));

        res.json("Deleted successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


// to get the CSV file that contains ECG data
router.get('/:filename', authorization, async function(req, res){
    const { filename } = req.params
    const csvFilePath=`${__dirname}\\..\\public\\${filename}`;

    csv().fromFile(csvFilePath)
            .then(jsonObj => {
                let data = []
                jsonObj.forEach(row => {
                    data.push(row["ECG"])
                })
                res.json({"ECG": data});
            });

});

module.exports = router;