const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const bcrypt = require("bcrypt");
const validInfo = require("../middleware/validinfo");


//get the admin
router.get("/:id", authorization, async(req, res) => {
    try {
        const { id } = req.params;

        const admin = await pool.query(`SELECT * FROM admin WHERE data_analyst_id='${ id }'`);
        res.json(admin.rows[0]);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update the admin
router.put("/:id", authorization, validInfo, async(req, res) => {
    try {
        const { id } = req.params;
        const { admin_password } = req.body;

        //Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(admin_password, salt);

        await Object.keys(req.body).forEach(
            key => {
                if(key==="admin_password"){
                    pool.query(`UPDATE admin SET admin_password='${bcryptPassword}' WHERE adminid='${id}'`);
                } else {
                    pool.query(`UPDATE admin SET ${key} = '${req.body[key]}' WHERE admin_id='${id}'`);
                }
            }
        );

        res.json("Updated successfully!");

    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;