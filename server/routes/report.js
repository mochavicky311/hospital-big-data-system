const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");


const formatDate = (date) => {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
};


//create monthly report data
const createNewData = async (date) => {

    try {
        // get latest report data based on date given
        let reportData = await pool.query(`SELECT * FROM management_info 
                                          WHERE date < date '${formatDate(date)}'
                                          ORDER BY date DESC;`);

        // create new report data of the data
        const { total_beds, total_discharges, current_doctors, total_admitted, current_patients,
                female, male, agegrp1, agegrp2, agegrp3, agegrp4, disease1, disease2, disease3,
                disease4, disease5, disease6, chinese, malay, indian, other } = reportData.rows[0];

        reportData = await pool.query(`INSERT INTO management_info 
                        (date, total_beds, total_discharges, current_doctors, total_admitted, current_patients, 
                        female, male, agegrp1, agegrp2, agegrp3, agegrp4, disease1, disease2, disease3, disease4, 
                        disease5, disease6, chinese, malay, indian, other)
                        VALUES ('${formatDate(date)}', ${total_beds}, ${total_discharges}, ${current_doctors}, 
                        ${total_admitted}, ${current_patients}, ${female}, ${male}, ${agegrp1}, ${agegrp2}, ${agegrp3},
                        ${agegrp4}, ${disease1}, ${disease2}, ${disease3}, ${disease4}, ${disease5}, ${disease6},
                        ${chinese}, ${malay}, ${indian}, ${other}) RETURNING *;`);

        return reportData;

    }catch (err) {
        console.log(err.message);
        return false;
    }
};


//get monthly report data
router.get("/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { year, month, day }  = req.params;

        let reportData = await pool.query(`SELECT * FROM management_info 
                                          WHERE date=date '${year}-${month}-${day}';`);

        // if there is no record of the requested date
        if(reportData.rows.length === 0){
            reportData = createNewData(new Date(year, month-1, day));

            if(!reportData)
                res.status(500).json("Server Error");
        }

        res.json(reportData.rows[0]);

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get total no of beds
router.get("/bed", authorization, async(req, res) => {
    try {

        const totalBeds = await pool.query(`SELECT total_beds FROM management_info ORDER BY date DESC;`);

        res.json(totalBeds.rows[0]);

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update monthly report data
router.put("/", authorization, async(req, res) => {

    const todayDate = new Date();
    const lastDayDate = formatDate(new Date(todayDate.getFullYear(), todayDate.getMonth()+1, 0));

    try {
        // check if data for the latest month
        let response = await pool.query(`SELECT * FROM management_info WHERE date=date '${lastDayDate}';`);

        // if it doesn't exist
        if(response.rows.length === 0) {
            response = createNewData(lastDayDate);

            if(!response)
                res.status(500).json("Server Error");
        }

        // update the data
        await Object.keys(req.body).forEach(
            key =>
                pool.query(`UPDATE management_info SET ${key}=${req.body[key]} WHERE date=date '${lastDayDate}';`)
        );

        res.json("Updated successfully!");

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;