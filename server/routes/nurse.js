const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const bcrypt = require("bcrypt");
const validInfo = require("../middleware/validinfo");


//create a nurse
router.post("/", authorization, async(req, res) => {
    try {
        const { nurse_name,
            nurse_email,
            nurse_password
        } = req.body;

        await pool.query(
            "INSERT INTO nurse " +
            "(nurse_name, nurse_email, dnurse_password) " +
            `VALUES('${nurse_name}', '${nurse_email}', '${nurse_password}'); `
        );

        res.json("Created successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all nurse
router.get("/", authorization, async(req, res) => {
    try {
        const allNurse = await pool.query("SELECT * FROM nurse;");
        res.json(allNurse.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get a nurse
router.get("/:id", authorization, async(req, res) => {
    try {
        const { id } = req.params;
        const nurse = await pool.query(`SELECT * FROM nurse WHERE nurse_id='${ id }';`);
        res.json(nurse.rows[0]);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update a nurse
router.put("/:nurse_id", authorization, validInfo, async(req, res) => {
    try {
        const { nurse_id } = req.params;
        const { nurse_password } = req.body;

        if(nurse_password === "nochange"){
            await Object.keys(req.body).filter(key => key !== "nurse_password").forEach(
                key =>
                    pool.query(`UPDATE nurse SET ${key} = '${req.body[key]}' WHERE nurse_id='${nurse_id}';`)
            );
        } else {
            //Bcrypt the user password
            const saltRound = 10;
            const salt = await bcrypt.genSalt(saltRound);
            const bcryptPassword = await bcrypt.hash(nurse_password, salt);

            await Object.keys(req.body).forEach(
                key => {
                    if(key==="nurse_password"){
                        pool.query(`UPDATE nurse SET nurse_password='${bcryptPassword}' WHERE nurse_id='${nurse_id}';`);
                    } else{
                        pool.query(`UPDATE nurse SET ${key} = '${req.body[key]}' WHERE nurse_id='${nurse_id}';`);
                    }
                }
            );
        }

        res.json("Updated successfully!");

    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//delete a nurse
router.delete("/:id", authorization, async (req, res) => {
    try {
        const { id } = req.params;
        await pool.query(`DELETE FROM nurse WHERE nurse_id='${id}';`);

        await pool.query(`DELETE FROM users WHERE user_id='${id}';`);

        res.json("Deleted successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;