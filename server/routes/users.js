const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");


//get all users
router.get("/", authorization, async(req, res) => {
    try {
        const allUsers = await pool.query("SELECT * FROM users;");
        res.json(allUsers.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get a user
router.get("/:id", authorization, async(req, res) => {
    try {
        const { id } = req.params;
        const user = await pool.query(`SELECT * FROM users WHERE user_id='${ id }';`);
        res.json(user.rows[0]);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;