const bcrypt = require("bcrypt");
const router = require("express").Router();
const jwtGenerator = require("../utils/jwtGenerator");
const validInfo = require("../middleware/validinfo");
const authorization = require("../middleware/authorization");  //to check whether the token is valid
const pool = require("../db");
const bmiAnalyzer = require("../utils/bmiAnalyzer");


//Register Patients
router.post("/registerPatient", validInfo, async (req, res) => {
    try {
        const { patient_name,
            patient_email,
            patient_password,
            patient_contact,
            med_id,
            patient_gender,
            patient_birth_year,
            patient_emgcy_no,
            patient_height_m,
            patient_weight_kg,
            patient_bmi,
            disease,
            is_vegan,
            patient_race,
            is_married,
            in_patient } = req.body;

        const date = `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;


        //Check if the patient email exists
        const user = await pool.query(`SELECT * FROM patients WHERE patient_email = '${patient_email}'`);

        //if exists
        if(user.rows.length !== 0) {
            return res.status(401).send("Patient already exist");
        }

        //Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(patient_password, salt);

        //Insert into db
        const id = await pool.query(
            "INSERT INTO patients " +
            "(patient_name, patient_email, patient_password, patient_contact, med_id, " +
            "patient_gender, patient_birth_year, patient_emgcy_no, disease, is_vegan, patient_race, is_married, in_patient) " +
            `VALUES('${patient_name}', '${patient_email}', '${bcryptPassword}', '${patient_contact}', '${med_id}', ` +
            `'${patient_gender}', ${patient_birth_year}, '${patient_emgcy_no}', `+
            `${disease}, ${is_vegan}, '${patient_race}', ${is_married}, ${in_patient}) RETURNING patient_id;`
        );

        const result = bmiAnalyzer(patient_bmi);

        await pool.query(
            "INSERT INTO patient_bmi_change " +
            "(patient_id, record_date, height_in_m, weight_in_kg, current_bmi, current_category, analytics) " +
            `VALUES ('${id.rows[0].patient_id}', '${date}', ${patient_height_m}, ${patient_weight_kg}, ${patient_bmi}, '${result.category}', '${result.analytics}');`
        );

        await pool.query(
            `INSERT INTO users (user_id, user_role) VALUES ('${id.rows[0].patient_id}', 'P');`
        );

        res.json("Created successfully!");

    }catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Login Patient
router.post("/loginPatient", validInfo, async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await pool.query(`SELECT *  FROM patients WHERE patient_email ='${email}'`);

        if(user.rows.length === 0){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Check id entered password is the same as the db password
        const validPassword = await bcrypt.compare(password, user.rows[0].patient_password);
        if(!validPassword){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Give the jwt token
        const token = jwtGenerator(user.rows[0].patient_id);
        const id = user.rows[0].patient_id;
        res.json({ token, id });

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Register Doctors
router.post("/registerDoctor", validInfo, async (req, res) => {
    try {
        const { med_name,
            med_email,
            med_password,
            med_contact
            } = req.body;

        //Check if the email exists
        const user = await pool.query(`SELECT * FROM doctor WHERE med_email='${med_email}'`);

        //if exists
        if(user.rows.length !== 0) {
            return res.status(401).send("Email already taken.");
        }

        //Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(med_password, salt);

        //Insert into db
        const id = await pool.query(
            "INSERT INTO doctor " +
            "(med_name, med_email, med_password, med_contact) " +
            `VALUES('${med_name}', '${med_email}', '${bcryptPassword}', '${med_contact}') RETURNING med_id;`
        );

        await pool.query(
            `INSERT INTO users (user_id, user_role) VALUES ('${id.rows[0].med_id}', 'D');`
        );

        res.json("Created successfully!");

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Login Doctor
router.post("/loginDoctor", validInfo, async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await pool.query(`SELECT * FROM doctor WHERE med_email ='${email}'`);

        if(user.rows.length === 0){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Check id entered password is the same as the db password
        const validPassword = await bcrypt.compare(password, user.rows[0].med_password);
        if(!validPassword){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Give the jwt token
        const token = jwtGenerator(user.rows[0].med_id);
        const id = user.rows[0].med_id;
        res.json({ token, id });

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Register Admin
router.post("/registerAdmin", validInfo, async (req, res) => {
    try {
        const { admin_name,
            admin_email,
            admin_password
        } = req.body;

        //Check if the email exists
        const user = await pool.query(`SELECT * FROM admin WHERE admin_email='${admin_email}'`);

        //if exists
        if(user.rows.length !== 0) {
            return res.status(401).send("Email already taken.");
        }

        //Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(admin_password, salt);

        //Insert into db
        const newAdmin = await pool.query(
            "INSERT INTO admin " +
            "(admin_name, admin_email, admin_password) " +
            `VALUES('${admin_name}', '${admin_email}', '${bcryptPassword}') RETURNING *;`
        );

        await pool.query(
            `INSERT INTO users (user_id, user_role) VALUES ('${id.rows[0].patient_id}', 'A');`
        );

        res.json("Created successfully!");

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Login Admin
router.post("/loginAdmin", validInfo, async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await pool.query(`SELECT * FROM admin WHERE admin_email ='${email}'`);

        if(user.rows.length === 0){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Check id entered password is the same as the db password
        const validPassword = await bcrypt.compare(password, user.rows[0].admin_password);
        if(!validPassword){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Give the jwt token
        const token = jwtGenerator(user.rows[0].admin_id);
        const id = user.rows[0].admin_id;
        res.json({ token, id });

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Register nurse
router.post("/registerNurse", validInfo, async (req, res) => {
    try {
        const { nurse_name,
            nurse_email,
            nurse_password
        } = req.body;

        //Check if the email exists
        const user = await pool.query(`SELECT * FROM nurse WHERE nurse_email='${nurse_email}'`);

        //if exists
        if(user.rows.length !== 0) {
            return res.status(401).send("Email already taken.");
        }

        //Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(nurse_password, salt);

        //Insert into db
        const id = await pool.query(
            "INSERT INTO nurse " +
            "(nurse_name, nurse_email, nurse_password) " +
            `VALUES('${nurse_name}', '${nurse_email}', '${bcryptPassword}') RETURNING nurse_id;`
        );

        await pool.query(
            `INSERT INTO users (user_id, user_role) VALUES ('${id.rows[0].nurse_id}', 'N');`
        );

        res.json("Created successfully!");

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


//Login nurse
router.post("/loginNurse", validInfo, async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await pool.query(`SELECT * FROM nurse WHERE nurse_email ='${email}'`);

        if(user.rows.length === 0){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Check id entered password is the same as the db password
        const validPassword = await bcrypt.compare(password, user.rows[0].nurse_password);
        if(!validPassword){
            return res.status(401).json("Email or password is incorrect.");
        }

        //Give the jwt token
        const token = jwtGenerator(user.rows[0].nurse_id);
        const id = user.rows[0].nurse_id;
        res.json({ token, id });

    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
})


router.get("/isVerify", authorization, async (req, res) => {
   try {
        res.json(true);
   } catch (err) {
       console.error(err.message);
       res.status(500).send("Server Error");
   }
});

module.exports = router;