const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const bmiAnalyzer = require("../utils/bmiAnalyzer");


//create bmi analytics for a patient
router.post("/", authorization, async(req, res) => {
    try {

        const { patient_id,
            record_date,
            height_in_m,
            weight_in_kg,
            current_bmi,
            last_bmi,
            last_category }  = req.body;

        // get latest BMI analytics
        const result = bmiAnalyzer(current_bmi, last_category);

        await pool.query(
            "INSERT INTO patient_bmi_change " +
            "(patient_id, record_date, height_in_m, weight_in_kg, current_bmi, last_bmi, current_category, last_category, analytics) " +
            `VALUES ('${patient_id}', '${record_date}', ${height_in_m}, ${weight_in_kg}, ${current_bmi}, ${last_bmi}, '${result.category}', '${last_category}', '${result.analytics}');`
        );

        res.json("Created successfully!");

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});

//get bmi analytics for a patient
router.get("/:id/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { id, year, month, day }  = req.params;
        const allBmiData = await pool.query(`SELECT * FROM patient_bmi_change 
                                            WHERE patient_id='${ id }'
                                            AND record_date <= '${year}-${month}-${day}'
                                            ORDER BY id DESC;`);

        res.json(allBmiData.rows[0]);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;