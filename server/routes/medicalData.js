const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");


//get medical data for a patient
router.get("/:id/:year/:month/:day", authorization, async(req, res) => {
    try {
        const { id, year, month, day }  = req.params;
        const allMedicalData = await pool.query(`SELECT * FROM medical_data 
                                            WHERE patient_id='${ id }'
                                            AND record_date BETWEEN (date '${year}-${month}-${day}' - integer '6') AND (date '${year}-${month}-${day}' + integer '1')
                                            ORDER BY med_data_id`);
        res.json(allMedicalData.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;