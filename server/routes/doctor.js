const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const bcrypt = require("bcrypt");
const validInfo = require("../middleware/validinfo");


//get all doctors
router.get("/", authorization, async(req, res) => {
    try {
        const allDoctors = await pool.query("SELECT * FROM doctor ORDER BY med_name");
        res.json(allDoctors.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get a doctor
router.get("/:med_id", authorization, async(req, res) => {
    try {
        const { med_id } = req.params;
        const doctor = await pool.query(`SELECT * FROM doctor WHERE med_id='${ med_id }'`);
        res.json(doctor.rows[0]);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update a doctor
router.put("/:id", authorization, validInfo, async(req, res) => {

    try {
        const { id } = req.params;
        const { med_password } = req.body;

        if(med_password === "nochange"){
            await Object.keys(req.body).filter(key => key !== "med_password").forEach(
                key =>
                    pool.query(`UPDATE doctor SET ${key} = '${req.body[key]}' WHERE med_id='${id}'`)
            );
        } else {
            //Bcrypt the user password
            const saltRound = 10;
            const salt = await bcrypt.genSalt(saltRound);
            const bcryptPassword = await bcrypt.hash(med_password, salt);

            await Object.keys(req.body).forEach(
                key => {
                    if(key==="med_password"){
                        pool.query(`UPDATE doctor SET med_password='${bcryptPassword}' WHERE med_id='${id}'`);
                    } else{
                        pool.query(`UPDATE doctor SET ${key} = '${req.body[key]}' WHERE med_id='${id}'`);
                    }
                }
            );
        }

        res.json("Updated successfully!");

        } catch (err) {
            console.log(err.message);
            res.status(500).json("Server Error");
        }
});


//delete a doctor
router.delete("/:id", authorization, async (req, res) => {
    try {
        const { id } = req.params;
        await pool.query(`DELETE FROM doctor WHERE med_id='${id}'`);

        await pool.query(
            `DELETE FROM users WHERE user_id='${id}';`
        );

        res.json("Deleted successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get all patients under a doctor
router.get("/:med_id/patients", authorization, async(req, res) => {
    try {
        const { med_id }  = req.params;
        const allPatients = await pool.query(`SELECT * FROM patients WHERE med_id='${ med_id }'`);
        res.json(allPatients.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;