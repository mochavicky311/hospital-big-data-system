const bcrypt = require("bcrypt");
const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const validInfo = require("../middleware/validinfo");


//get all patients
router.get("/", authorization, async(req, res) => {
    try {
        const allPatients = await pool.query("SELECT * FROM patients ORDER BY patient_name");
        res.json(allPatients.rows);
    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//get a patient
router.get("/:id", authorization, async (req, res) => {
    try {
        const { id }  = req.params;
        const patient = await pool.query(`SELECT * FROM patients WHERE patient_id='${ id }'`);
        res.json(patient.rows[0]);

    }catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


//update a patient
router.put("/:id", authorization, validInfo, async (req, res) => {
    try {
        const { id } = req.params;
        const { patient_password } = req.body;


        if(patient_password === "nochange"){
            await Object.keys(req.body).filter(key => key !== "patient_password").forEach(
                key =>
                    pool.query(`UPDATE patients SET ${key} = '${req.body[key]}' WHERE patient_id='${id}'`)
            );
        } else {
            //Bcrypt the user password
            const saltRound = 10;
            const salt = await bcrypt.genSalt(saltRound);
            const bcryptPassword = await bcrypt.hash(patient_password, salt);

            await Object.keys(req.body).forEach(
                key => {
                    if(key==="patient_password"){
                        pool.query(`UPDATE patients SET patient_password='${bcryptPassword}' WHERE patient_id='${id}'`);
                    } else{
                        pool.query(`UPDATE patients SET ${key} = '${req.body[key]}' WHERE patient_id='${id}'`);
                    }
                }
            );
        }

        res.json("Updated successfully!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});


// discharge a patient
router.put("/discharge/:id/:value", authorization, async (req, res) => {
    try {
        const { id, value } = req.params;
        await pool.query(`UPDATE patients SET in_patient=${value} WHERE patient_id='${id}'`);

        res.json("Success!");
    } catch (err) {
        console.log(err.message);
        res.status(500).json("Server Error");
    }
});

module.exports = router;