function habitAnalyzer(change, previous, current, type) {

    let category = "";
    let analytics = "";
    let result = {};
    let keyword = "";
    if(type === "diet" || type === "sleep")
        keyword = "healthy"
    else if(type === "exercise")
        keyword = "effort"
    else
        keyword = "changes"


    // analytics
    const veryHighChangePositive = `The patient's ${type} habit has shown massive improvement, where the level of 
    ${keyword} increased from ${previous} to ${current}.`;
    const veryHighChangeNegative = `The patient's ${type} habit has changed massively. The The level of ${keyword} 
    decreased from ${previous} to ${current}.`;
    const highChangePositive = `The patient's ${type} habit has a great improvement. The level of ${keyword} increased 
    from ${previous} to ${current}.`;
    const highChangeNegative = `The patient's ${type} habit has shown a great change. The level of ${keyword} decreased 
    from ${previous} to ${current}.`;
    const mediumChangePositive = `The patient's ${type} habit shown some improvement. The level of ${keyword} increased 
    from ${previous} to ${current}.`;
    const mediumChangeNegative = `The level of ${keyword} of the patient's ${type} habit decreased from ${previous} 
    to ${current}.`;
    const littleChangePositive = `The ${type} habit of the patient has shown a little improvements compared to 
    yesterday's. The level of ${keyword} improved from ${previous} to ${current}.`;
    const littleChangeNegative = `The ${type} habit of the patient has shown a little change compared to 
    yesterday's. The level of ${keyword} decreased from ${previous} to ${current}.`;
    const noChange = `The ${type} habit has no change compared to yesterday. The level of ${keyword} remains at 
    ${previous}.`;
    const noChangeOverall = `The ${type} habit has no change compared to yesterday. `

    // get change category and analytics
    if(change === 4){
        category = "Very High Change";
        analytics = veryHighChangePositive;
    }
    else if(change === -4){
        category = "Very High Change";
        analytics = veryHighChangeNegative;
    }
    else if(change === 3){
        category = "High Change";
        analytics = highChangePositive;
    }
    else if(change === -3){
        category = "High Change";
        analytics = highChangeNegative;
    }
    else if(change === 2){
        category = "Medium Change";
        analytics = mediumChangePositive;
    }
    else if(change === -2){
        category = "Medium Change";
        analytics = mediumChangeNegative;
    }
    else if(change === 1){
        category = "Little Change";
        analytics = littleChangePositive;
    }
    else if(change === -1){
        category = "Little Change";
        analytics = littleChangeNegative;
    }
    else {
        category = "No Change";
        if(type === "overall")
            analytics = noChangeOverall;
        else
            analytics = noChange;
    }

    result['category'] = category;
    result['analytics'] = analytics;

    return result;
}

module.exports = habitAnalyzer;