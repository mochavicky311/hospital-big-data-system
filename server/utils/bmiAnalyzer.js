function bmiAnalyzer(bmi, last_category="") {

    let category = "";
    let analytics = "";
    let result = {};

    // analytics
    const severeThin = "Based on the BMI, the patient is severely thin. The patient may have higher risk " +
        "of developing many health problems associated with underweight or having poor nutrition. " +
        "The patient should improve own habits to gain healthy weight. ";
    const moderateThin = "Based on the BMI, the patient is moderately thin. Although this does not indicate a " +
        "high risk of developing heart disease, the patient may have other risks such as " +
        "malnutrition and decrease in immune function. The patient should change own habits " +
        "for a healthy weight.";
    const mildThin = "Based on the BMI, the patient is mildly thin. Although the patient does not have a " +
        "high risk of developing heart disease,  it may cause other risks. Therefore, the patient " +
        "is advised to gain some weights to maintain healthy.";
    const normal = "Based on the BMI, the patient has normal weight. This indicates that the patient is " +
        "healthy and has a low risk of developing heart disease. The patient should continue " +
        "current habits to maintain this normal weight. ";
    const overweight = "Based on the BMI, the patient is overweight. This indicates that the patient has a " +
        "moderate risk of developing heart disease. The patient needs to lose some weights to " +
        "reduce the risk.";
    const obese1 = "Based on the BMI, the patient is obese (Class I). The patient has high risk of " +
        "developing heart disease. Hence, the patient needs to lose more weights to reduce the " +
        "risk.";
    const obese2 = "Based on the BMI, the patient is obese (Class II). The patient has a very high " +
        "risk of developing heart disease. Hence, the patient needs to lose more weights to " +
        "reduce the risk.";
    const obese3 = "Based on the BMI, the patient is obese (Class III). The patient has a extremely " +
        "high risk of developing heart disease. Hence, the patient needs to lose weights " +
        "immediately to reduce the risk.";
    const noChange = "The BMI category has no change. ";


    // get bmi category
    if(bmi < 16)
        category = "Severe Thinness";
    else if(bmi >= 16 && bmi < 17)
        category = "Moderate Thinness";
    else if(bmi >= 17 && bmi < 18.5)
        category = "Mild Thinness";
    else if(bmi >= 18.5 && bmi < 25)
        category = "Normal";
    else if(bmi >= 25 && bmi < 30)
        category = "Overweight";
    else if(bmi >= 30 && bmi < 35)
        category = "Obese Class I";
    else if(bmi >= 35 && bmi < 40)
        category = "Obese Class II";
    else
        category = "Obese Class III";


    // get bmi analytics
    if (last_category === ""){
        if(category==="Severe Thinness")
            analytics = severeThin;
        else if(category==="Moderate Thinness")
            analytics = moderateThin;
        else if(category==="Mild Thinness")
            analytics = mildThin;
        else if(category==="Normal")
            analytics = normal;
        else if(category==="Overweight")
            analytics = overweight;
        else if(category==="Obese Class I")
            analytics = obese1;
        else if(category==="Obese Class II")
            analytics = obese2;
        else if(category==="Obese Class III")
            analytics = obese3;

    } else if(last_category === category) {
        if(category==="Severe Thinness")
            analytics = noChange + severeThin;
        else if(category==="Moderate Thinness")
            analytics = noChange + moderateThin;
        else if(category==="Mild Thinness")
            analytics = noChange + mildThin;
        else if(category==="Normal")
            analytics = noChange + normal;
        else if(category==="Overweight")
            analytics = noChange + overweight;
        else if(category==="Obese Class I")
            analytics = noChange + obese1;
        else if(category==="Obese Class II")
            analytics = noChange + obese2;
        else if(category==="Obese Class III")
            analytics = noChange + obese3;
    } else if(category==="Normal"){
        analytics = `The BMI changes from ${last_category} to normal. Having normal weight ` +
        "indicates that the patient is healthy and has a low risk of developing heart disease. The " +
        "patient should continue current habits to maintain this normal weight. ";
    } else {
        if(category==="Severe Thinness")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + severeThin;
        else if(category==="Moderate Thinness")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + moderateThin;
        else if(category==="Mild Thinness")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + mildThin;
        else if(category==="Normal")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + normal;
        else if(category==="Overweight")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + overweight;
        else if(category==="Obese Class I")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + obese1;
        else if(category==="Obese Class II")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + obese2;
        else if(category==="Obese Class III")
            analytics = `The BMI has shown changes since the last record, which is ${last_category}. ` + obese3;
    }

    result['category'] = category;
    result['analytics'] = analytics;

    return result;
}

module.exports = bmiAnalyzer;