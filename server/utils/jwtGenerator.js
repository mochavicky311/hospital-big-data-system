const jwt = require("jsonwebtoken");
require('dotenv').config();

function jwtGenerator(user_id) {
    const payload = {
        user_id: user_id,
        iat: Math.floor(Date.now() / 1000),
        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 12)
    }

    return jwt.sign(JSON.stringify(payload), process.env.jwtSecret);
}

module.exports = jwtGenerator;