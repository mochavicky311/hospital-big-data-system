const express = require('express');
const app = express();
const cors = require('cors');

//middleware
app.use(cors());
app.use(express.json()); //allows to use ;req.body
app.use(express.static('public')); //to access the files in public folder

//Routes
//register and login routes
app.use("/auth", require("./routes/jwtAuth"));
//resources routes
app.use("/users", require("./routes/users"));
app.use("/doctor", require("./routes/doctor"));
app.use("/patients", require("./routes/patients"));
app.use("/diseases", require("./routes/diseases"));
app.use("/medicalData", require("./routes/medicalData"));
app.use("/admin", require("./routes/admin"));
app.use("/nurse", require("./routes/nurse"));
app.use("/upload", require("./routes/fileUpload"));
app.use("/ecg", require("./routes/ecg"));
app.use("/heartsound", require("./routes/hearbeatSound"));
app.use("/bmi", require("./routes/bmi"));
app.use("/habit", require("./routes/habitData"));
app.use("/appointment", require("./routes/appointments"));
app.use("/report", require("./routes/report"));


app.listen(5000, () => {
    console.log("server has started on port 5000");
});

